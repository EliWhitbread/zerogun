﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsPanel : MonoBehaviour
{    
    public void ClosePanel()
    {
        Time.timeScale = 1.0f;
        gameObject.SetActive(false);
    }

}
