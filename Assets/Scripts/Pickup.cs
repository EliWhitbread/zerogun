﻿using UnityEngine;
using System.Collections;

public class Pickup : MonoBehaviour
{

    public int hitPoints, score;
    public GameObject player;
    public float asteroidAutoDestroyDistance;

    public enum PickupType { Crystal, Shield, Weapon, SlowTime };
    public PickupType puType = PickupType.Shield;
    [Header("Weapon Type only applies if PickupType = Weapon")]
    //public WeaponTypes myWeaponType;
    public StackableWeapons thisWeaponType;
    StackableWeaponPickupType thisWeaponPickupType;

    //SpriteRenderer mySpriteRenderer;
    ////pickup sprites
    //public Sprite gun_Basic, gun_Big, gun_Multi;

    float customTick = 0.2f, curTime = 0.0f;
    Animator anim;

    //fade out
    SpriteRenderer spRenderer;
    BoxCollider2D boxCollider;
    Transform thisTransform;

    float crystalRotSpeed = 10.0f;
    //private void Start()
    //{
    //    mySpriteRenderer = GetComponent<SpriteRenderer>();
    //}

    void OnEnable()
    {
        if(anim == null)
        {
            anim = GetComponent<Animator>();
        }
        
        //if(mySpriteRenderer == null)
        //{
        //    mySpriteRenderer = GetComponent<SpriteRenderer>();
        //}

        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }

        if(spRenderer == null)
        {
            spRenderer = gameObject.GetComponent<SpriteRenderer>();
        }
        if(boxCollider == null)
        {
            boxCollider = gameObject.GetComponent<BoxCollider2D>();
        }

        if(thisTransform == null)
        {
            thisTransform = gameObject.GetComponent<Transform>();
        }

        boxCollider.enabled = true;
        spRenderer.color = Color.white;
        spRenderer.sortingOrder = 0;
        thisTransform.localScale = Vector3.one;

        //swap sprite and stats
        switch (puType)
        {
            case PickupType.Crystal:
                anim.SetTrigger("Crystal");
                crystalRotSpeed = Random.Range(-15.0f, 15.0f);
                break;
            case PickupType.SlowTime:
                anim.SetTrigger("SlowTime");
                break;
            case PickupType.Shield:
                anim.SetTrigger("Shield");
                break;
            case PickupType.Weapon:
                SwitchGunPUSprite();
                break;
            default:
                break;
        }
    }

    void SwitchGunPUSprite()
    {
        switch (thisWeaponType)
        {
            case StackableWeapons.BIG_SHOT:
                thisWeaponPickupType = StackableWeaponPickupType.SIZE_TYPE;
                anim.SetTrigger("BigShot");
                break;
            case StackableWeapons.LASER:
                thisWeaponPickupType = StackableWeaponPickupType.SHOT_TYPE;
                anim.SetTrigger("Laser");
                break;
            case StackableWeapons.SPREAD_SHOT:
                thisWeaponPickupType = StackableWeaponPickupType.SPREAD_TYPE;
                anim.SetTrigger("SpreadShot");
                break;
            default:
                break;
        }
        
    }

    void FixedUpdate()
    {
        curTime += Time.deltaTime;

        if (player != null && curTime >= customTick)
        {
            curTime = 0.0f;
            Vector2 offset = player.transform.position - transform.position;
            float sqrLength = offset.sqrMagnitude;
            if (sqrLength > asteroidAutoDestroyDistance * asteroidAutoDestroyDistance)
            {
                gameObject.SetActive(false);
            }
        }
    }

    private void Update()
    {
        if(puType == PickupType.Crystal)
        {
            transform.Rotate(Vector3.forward, crystalRotSpeed * Time.deltaTime);
        }
    }

    public void OnPlayerCollect()
    {
        switch (puType)
        {
            case PickupType.Crystal:
                GameManager.curGameManager.UpdatePickups(2, Random.Range(20, 31));
                break;
            case PickupType.SlowTime:
                GameManager.curGameManager.AlterTimeScale(0.2f, 3.0f);
                //GameManager.curGameManager.UpdatePickups(1, 100);
                //GameManager.curGameManager.activateAmmoRecharge = true;
                break;
            case PickupType.Shield:
                GameManager.curGameManager.UpdatePickups(0, 1);
                break;
            case PickupType.Weapon:

                //PlayerGunManager.curPlayerGunManager.SetupWeapon(thisWeaponPickupType, thisWeaponType);
                WeaponManager.curWeaponManager.AddWeaponPickUp(thisWeaponType);

                //foreach(WeaponStats stats in GameManager.curGameManager.availableWeapons)
                //{
                //    //if(stats.thisWeaponType == this.thisWeaponType)
                //    //{
                //    //    PlayerGunManager.curPlayerGunManager.WeaponStats = stats;
                //    //    break;
                //    //}
                //}
                break;
            default:
                break;
        }

        GameManager.curGameManager.UpdatePlayerScore(score);
        //gameObject.SetActive(false);
        StartCoroutine(FadeOut());
    }

    IEnumerator FadeOut()
    {
        spRenderer.sortingOrder = 20;
        Color _c = spRenderer.color;
        _c.a -= 0.3f;
        boxCollider.enabled = false;
        float _t = Random.Range(1.0f, 2.0f);
        float _fadeAmt = 1.0f / (_t * 100);
        while (_t >= 0.0f)
        {
            _t -= Time.unscaledDeltaTime;
            _c.a -= _fadeAmt;
            spRenderer.color = _c;
            
            thisTransform.localScale += Vector3.one * (Time.unscaledDeltaTime / 2);

            yield return null;
        }
        gameObject.SetActive(false);
    }
    //void OnTriggerEnter2D(Collider2D other)
    //{
    //    if (other.CompareTag("Player"))
    //    {
    //        switch (puType)
    //        {
    //            case PickupType.Crystal:
    //                GameManager.curGameManager.UpdatePickups(2, 1);
    //                break;
    //            case PickupType.SlowTime:
    //                GameManager.curGameManager.AlterTimeScale(0.2f, 3.0f);
    //                //GameManager.curGameManager.UpdatePickups(1, 100);
    //                //GameManager.curGameManager.activateAmmoRecharge = true;
    //                break;
    //            case PickupType.Shield:
    //                GameManager.curGameManager.UpdatePickups(0, 1);
    //                break;
    //            case PickupType.Weapon:

    //                //PlayerGunManager.curPlayerGunManager.SetupWeapon(thisWeaponPickupType, thisWeaponType);
    //                WeaponManager.curWeaponManager.AddWeaponPickUp(thisWeaponType);

    //                //foreach(WeaponStats stats in GameManager.curGameManager.availableWeapons)
    //                //{
    //                //    //if(stats.thisWeaponType == this.thisWeaponType)
    //                //    //{
    //                //    //    PlayerGunManager.curPlayerGunManager.WeaponStats = stats;
    //                //    //    break;
    //                //    //}
    //                //}
    //                break;
    //            default:
    //                break;
    //        }

    //        GameManager.curGameManager.UpdatePlayerScore(score);
    //        gameObject.SetActive(false);
    //    }
    //}

}
