﻿
// Project no longer using ADS
/*
using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;


public class UnityAdManager : MonoBehaviour {

    public static UnityAdManager curAdManager { get; private set; }

    int bonusCrystalsForWatchingAd;

    string gameId = "";

    public int BonusCrystals
    {
        get { return bonusCrystalsForWatchingAd; }
        set { bonusCrystalsForWatchingAd = value; }
    }

    void Awake()
    {
        if(curAdManager != null && curAdManager != this)
        {
            Destroy(gameObject);
            return;
        }
        curAdManager = this;
    }

    private void Start()
    {
        //required only because inbult ads extension not working (editor bug or setup conflicts?) - had to import Unity Ads package. (IMPORTANT! - requires "enable built-in Ads extension" in services/ads to be false)
#if UNITY_IOS
    gameId = "1548069";
#elif UNITY_ANDROID
    gameId = "1548068";
#endif
        //if(Advertisement.isSupported)
        //{
        //    Advertisement.Initialize(gameId, true);
        //}
    }

    /// <summary>
    /// Show an ad to the player - Default = "Reward" (not skippable)
    /// </summary>
    public void ShowRewardAd()
    {
        Test_BypassAds();
        //ShowOptions options = new ShowOptions();
        //options.resultCallback = AdCallbackHandler;

        //if (Advertisement.IsReady())
        //{
        //    Advertisement.Show("rewardedVideo", options);
        //}
    }

    //void AdCallbackHandler(ShowResult results)
    //{
    //    switch (results)
    //    {
    //        case ShowResult.Failed:
    //            GameManager.curGameManager.RespawnPlayer();
    //            break;
    //        case ShowResult.Skipped:
    //            UIManager.curUIManager.DisplayPopUpMenu(true);
    //            break;
    //        case ShowResult.Finished:
    //            GameManager.curGameManager.UpdatePickups(2, bonusCrystalsForWatchingAd);
    //            GameManager.curGameManager.PlayerWatchedAd();
    //            GameManager.curGameManager.RespawnPlayer();
    //            break;
    //        default:
    //            break;
    //    }
    //}

    void Test_BypassAds()
    {
        GameManager.curGameManager.UpdatePickups(2, bonusCrystalsForWatchingAd);
        GameManager.curGameManager.PlayerWatchedAd();
        GameManager.curGameManager.RespawnPlayer();
    }
}
*/
