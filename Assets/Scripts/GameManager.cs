﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager curGameManager;

    [SerializeField] protected GameState curGameState;

    [SerializeField] PlayerShipScriptableObject currentPlayerShipData;

    public GameObject player, ammoRechargeEffect;

    public float timeAlive, maxPlayerShieldTime, ammoRechargeTime;

    public bool activateAmmoRecharge;
    public GameObject ship;

    public int playerScore, playerCrystals, playerAmmo, playerMaxAmmo, watchAdsCrystalBonus, watchedAdsBeforeBonusIncriment;
    float playerShieldTime, curRechargeTime;
    int scoreMultiplyer, watchedAds;
    bool playerAlive, playerHasControl;

    [Header("Ships")]
    //public List<ShipSprites> shipSprites = new List<ShipSprites>();
    PlayerShipManager shipManager;
    [SerializeField] int playerShipSelectionID = 0;
    public bool playerSelectedShip;

    [Header("Weapons")]
    public List<WeaponStats> availableWeapons;

    [Header("Menu Messages")]
    [SerializeField] string[] menuMessages;

    public GameState CurrentGameState
    {
        get { return curGameState; }
        set { curGameState = value; SwitchGameState(curGameState); }
    }

    public bool PlayerAlive
    {
        get { return playerAlive; }
        set { playerAlive = true; }
    }

    public int PlayerShipSelectionID
    {
        set { playerShipSelectionID = value; playerSelectedShip = true; }
    }

    void Awake()
    {        
        if(curGameManager != null && curGameManager != this)
        {
            Destroy(curGameManager);
        };
        curGameManager = this;
        Screen.orientation = ScreenOrientation.AutoRotation;
        ammoRechargeEffect.SetActive(false);
    }

    void Start()
    {
        if (player != null)
        {
            Destroy(player);
        }

        shipManager = GameObject.FindGameObjectWithTag("ShipManager").GetComponent<PlayerShipManager>();

        RespawnPlayer();                

        CurrentGameState = GameState.StartMenu;
        
    }

    public void LoadPlayerShipData()
    {
        if(shipManager == null)
        {
            shipManager = GameObject.FindGameObjectWithTag("ShipManager").GetComponent<PlayerShipManager>();
        }
        if(currentPlayerShipData == null || playerSelectedShip == false)
        {
            currentPlayerShipData = shipManager.GetShipData(true);
        }
        else
        {
            int _sel = PersistentData.PlayerShipSelection;
            currentPlayerShipData = shipManager.GetShipData(!playerSelectedShip, _sel);
        }


        //change player go to transform or PlayerController...

        //PlayerGunManager.curPlayerGunManager.ProjectileSpawnOffset = currentPlayerShipData.projectileFirePointOffset;
        WeaponManager.curWeaponManager.InitWeaponManager();

        //set player ship info
        maxPlayerShieldTime = currentPlayerShipData.shieldActiveTime;
        //PlayerController _playerController = player.GetComponent<PlayerController>();
        PlayerController.curPlayerController.CurrentShipID = currentPlayerShipData.shipID;
        PlayerController.curPlayerController.MoveSpeed = currentPlayerShipData.moveSpeed;
        PlayerController.curPlayerController.slowDownMoveMultiplyer = currentPlayerShipData.slowDownMoveMultiplyer;
        PlayerController.curPlayerController.floatSpeed = currentPlayerShipData.floatSpeed;
        PlayerController.curPlayerController.floatSpeedIncrementor = currentPlayerShipData.floatSpeedIncrement;
        //PlayerController.curPlayerController.shieldCooldownTime = currentPlayerShipData.shieldActiveTime;
        PlayerController.curPlayerController.shipRotationSpeed = currentPlayerShipData.shipRotationSpeed;
        PlayerController.curPlayerController.shipInertiaAmount = currentPlayerShipData.shipInertiaAmount;
        PlayerController.curPlayerController.crashDamage = currentPlayerShipData.crashDamage;
        //PlayerController.curPlayerController.projectileSpawnPoint = PlayerController.curPlayerController.gameObject.GetComponentInChildren<ProjectileSpawnPointInit>().gameObject;
        PlayerController.curPlayerController.ShieldAnimationId = currentPlayerShipData.shieldSpriteAnimID;
        PlayerController.curPlayerController.oxygenUseRate = currentPlayerShipData.shipOxygenDecrimentRate;
        PlayerController.curPlayerController.ResetProjectileSpawnPointPos(currentPlayerShipData.projectileFirePointOffset.y);

        //set player sprite
        player.GetComponent<SpriteRenderer>().sprite = currentPlayerShipData.mainShipSprite;

        //set player collider
        player.GetComponent<PolygonCollider2D>().SetPath(0,currentPlayerShipData.mainShipPolygonCollider);

        UIManager.curUIManager.UpdateShieldMaxValue(currentPlayerShipData.shieldActiveTime);
        UIManager.curUIManager.UpdateShieldSlider(maxPlayerShieldTime);

    }

    void Update()
    {
        if (playerAlive == true && playerHasControl == true)
        {
            timeAlive += Time.deltaTime;

            //timeText.text = "Time Alive: " + Mathf.Floor(timeAlive / 60).ToString("00") + ":" + Mathf.Floor(timeAlive % 60).ToString("00");
            UIManager.curUIManager.UpdateTimeText(timeAlive);

            if (playerShieldTime > 0)
            {
                playerShieldTime -= Time.deltaTime;
                UIManager.curUIManager.UpdateShieldSlider(playerShieldTime);

                if (playerShieldTime <= 0)
                {
                    //PlayerController.curPlayerController.ShieldCoolDown = true;
                    PlayerController.curPlayerController.ActivateShield(false);
                    //UIManager.curUIManager.SetShieldUiImageAlpha(false);
                }
            }
            //if (activateAmmoRecharge == true)
            //{
            //    curRechargeTime -= Time.deltaTime;
            //    if (PlayerGunManager.curPlayerGunManager.PlayerAmmo < 20)
            //    {
            //        if (curRechargeTime <= 0)
            //        {
            //            PlayerGunManager.curPlayerGunManager.AddAmmo(1);
            //            curRechargeTime = ammoRechargeTime;
            //        }
            //    }
            //    else
            //    {
            //        curRechargeTime = ammoRechargeTime;
            //        activateAmmoRecharge = false;
            //        ammoRechargeEffect.SetActive(false);
            //    }
            //}


        }
    }

    //void UpdateMaxShieldTime(float shTime)
    //{
    //    maxPlayerShieldTime += shTime;
    //    UIManager.curUIManager.UpdateShieldSlider(maxPlayerShieldTime);
    //}

    public void UpdatePlayerScore(int amount)
    {
        if (CurrentGameState != GameState.Play)
        {
            //do not update score unless player has control
            return;
        }
        playerScore += amount;
        //scoreText.text = "Score: " + playerScore.ToString();
        UIManager.curUIManager.UpdateScoreText(playerScore);
    }

    //void UpdateAmmoDisplay(int amt, bool active)
    //{

    //    //ammoText.text = active == true ? "Ammo: " + amt.ToString() : "";
    //    UIManager.curUIManager.UpdateAmmoText(amt, active);
    //}

    /// <summary>
    /// Update the player's Pickup collections. "id": 0 = Shield, 1 = Ammo, 2 = Crystal
    /// </summary>
    /// <param name="id"></param>
    /// <param name="value"></param>
    public void UpdatePickups(int id, int value)
    {
        switch (id)
        {
            case 0:
                playerShieldTime = maxPlayerShieldTime;
                PlayerController.curPlayerController.ActivateShield(true);
                //UIManager.curUIManager.SetShieldUiImageAlpha(true);
                break;
            case 1:
                //PlayerGunManager.curPlayerGunManager.AddAmmo(value);
                activateAmmoRecharge = false;
                break;
            case 2:
                if(player.activeInHierarchy)
                {
                    PlayerController.curPlayerController.OxygenLevel = (float)value;
                }
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// update the current Score Multiplyer based on the Player move speed
    /// </summary>
    /// <param name="curSpeed"></param>
    public void SetScoreMultiplyer(float curSpeed)
    {
        scoreMultiplyer = (int)Mathf.Clamp(curSpeed, 1.0f, 10.0f);
        //scoreMultiplyerText.text = "x" + scoreMultiplyer.ToString();
        UIManager.curUIManager.UpdateScoreMultiplyerText(scoreMultiplyer);
    }

    //slow time scale
    public void AlterTimeScale(float newTimeScale, float delay)
    {
        
        //Time.timeScale = newTimeScale;
        StartCoroutine(AdjustTimescale(newTimeScale, delay));
    }

    IEnumerator AdjustTimescale(float tScale, float delay)
    {
        Time.timeScale = tScale;
        yield return new WaitForSecondsRealtime(delay);
        Time.timeScale = 1.0f;
    }

    //player died
    public void TriggerPlayerDeath(bool showMessage, int sel)
    {
        AudioManager.audioManager.PlayAudio(AudioEffectClip.PlayerDeath);
        PlayerAlive = false;
        SetScoreMultiplyer(1.0f);
        player.SetActive(false);
        WeaponManager.curWeaponManager.CanShoot = false;
        UIManager.curUIManager.DisplayPopUpMenu(true, showMessage == true? menuMessages[sel] : "");
        CurrentGameState = GameState.Paused;        
        EnemySpawner.curEnemySpawner.CanSpawn = false;
        EnemySpawner.curEnemySpawner.ClearSpawnedObjects(false);
        RespawnPlayer();
    }

    //respawn the Player
    public void RespawnPlayer()
    {
        if(player == null)
        {
            player = Instantiate(Resources.Load("PlayerShip")) as GameObject;
        }

        player.SetActive(false);

        //PlayerGunManager.curPlayerGunManager.projectileSpawnPoint = player.GetComponentInChildren<ProjectileSpawnPointInit>().transform;
        //if (PlayerGunManager.curPlayerGunManager.projectileSpawnPoint == null)
        //{
        //    PlayerGunManager.curPlayerGunManager.projectileSpawnPoint = player.GetComponentInChildren<ProjectileSpawnPointInit>().transform;
        //}

        LoadPlayerShipData();

        //PlayerGunManager.curPlayerGunManager.ResetWeapon();

        timeAlive = 0.0f;
        scoreMultiplyer = 1;
        playerAlive = true;
       // RandomSpace.curRandomSpace.RandomiseSpace();
        playerShieldTime = maxPlayerShieldTime;

        //PlayerGunManager.curPlayerGunManager.PlayerAmmo = playerMaxAmmo;
        //UpdateMaxShieldTime(0);

        //update UI
        UIManager.curUIManager.UpdateScoreText(playerScore);
        UIManager.curUIManager.UpdateTimeText(timeAlive);
        UIManager.curUIManager.UpdateScoreMultiplyerText(scoreMultiplyer);
        //UIManager.curUIManager.UpdateCrystalText(playerCrystals);

        //timeText.text = "Time Alive: " + Mathf.Floor(timeAlive / 60).ToString("00") + ":" + Mathf.Floor(timeAlive % 60).ToString("00");
        //scoreText.text = "Score: " + playerScore.ToString();
        //scoreMultiplyerText.text = "x" + scoreMultiplyer.ToString();
        //crystalsText.text = playerCrystals.ToString();
        PlayerController.curPlayerController.SetPlayerState(curGameState);
        PlayerController.curPlayerController.PlayerCanMove = curGameState == GameState.Play ? true : false;
        //PlayerController.curPlayerController.ShieldCoolDown = false;
        EnemySpawner.curEnemySpawner.CanSpawn = true;
        player.SetActive(true);
        PlayerController.curPlayerController.ActivateShield(true);
        //UIManager.curUIManager.SetShieldUiImageAlpha(true);
        UIManager.curUIManager.UpdateShieldSlider(playerShieldTime);

    }

    private void RandomiseShip()
    {
        currentPlayerShipData = shipManager.GetShipData(true); //random ship for now
    }

    /// <summary>
    /// Reward the player for watching a "reward" ad
    /// </summary>
    //public void PlayerWatchedAd()
    //{
    //    watchedAds++;
    //    if (watchedAds >= watchedAdsBeforeBonusIncriment)
    //    {
    //        watchedAds = 0;
    //        watchAdsCrystalBonus++;
    //        UnityAdManager.curAdManager.BonusCrystals = watchAdsCrystalBonus;
    //    }
    //}

    /// <summary>
    /// set the current state of the game
    /// </summary>
    private void SwitchGameState(GameState newGameState)
    {
        switch (newGameState)
        {
            case GameState.StartMenu:
                playerHasControl = false;
                PlayerController.curPlayerController.PlayerCanMove = false;
                //PlayerGunManager.curPlayerGunManager.PlayerControlEnabled = false;
                EnemySpawner.curEnemySpawner.SpawnPickups = false;
                break;
            case GameState.Paused:
                playerHasControl = false;
                PlayerController.curPlayerController.PlayerCanMove = false;
                //PlayerGunManager.curPlayerGunManager.PlayerControlEnabled = false;
                EnemySpawner.curEnemySpawner.SpawnPickups = false;
                UIManager.curUIManager.FadeStatsUI(true);
                break;
            case GameState.Play:
                playerHasControl = true;
                PlayerController.curPlayerController.PlayerCanMove = true;
                //PlayerGunManager.curPlayerGunManager.PlayerControlEnabled = true;
                EnemySpawner.curEnemySpawner.SpawnPickups = true;
                UIManager.curUIManager.FadeStatsUI(false);
                break;
            case GameState.ShipSelection:

                PlayerAlive = false;
                SetScoreMultiplyer(1.0f);
                player.SetActive(false);
                WeaponManager.curWeaponManager.CanShoot = false;
                EnemySpawner.curEnemySpawner.CanSpawn = false;
                EnemySpawner.curEnemySpawner.ClearSpawnedObjects(false);

                playerHasControl = false;
                PlayerController.curPlayerController.PlayerCanMove = false;
                //PlayerGunManager.curPlayerGunManager.PlayerControlEnabled = false;
                EnemySpawner.curEnemySpawner.SpawnPickups = false;
                UIManager.curUIManager.FadeStatsUI(true);
                break;
            default:
                break;
        }
        PlayerController.curPlayerController.SetPlayerState(newGameState);
    }

    //Manual Garbage Collection
    public void ForceGarbageCollection(bool delayGC = false)
    {
        if(delayGC == false)
        {
            System.GC.Collect();
            return;
        }

        StartCoroutine(DelayedGarbageCollection());
    }

    IEnumerator DelayedGarbageCollection()
    {
        yield return new WaitForSecondsRealtime(0.1f);
        System.GC.Collect();
    }

}

[System.Serializable]
public struct ShipSprites
{
    public Sprite shipMain;
    public GameObject shipShield;
    //public Sprite shipHighlight;
    public PolygonCollider2D shipPolygonCollider2D;
    public Vector3 projectileSpawnPointOffset;
}

[System.Serializable]
public struct ShipDisplayInformation
{
    public string shipName;
    public bool shipUnlocked;
    public Sprite shipDisplaySprite;
    public int shipID;
}

[System.Serializable]
public struct WeaponStats
{
    public WeaponTypes thisWeaponType;
    public float fireDelay, baseSpeed, timeAlive, baseForceAppliedToPlayer;
    public int baseDamage;
    [Tooltip("Rotation from forward - Leave empty if only 1 projectile fired per shot")]
    public List<float> shotAngles;
}

public enum WeaponTypes
{
    Basic, BigGun, FanShot
}
