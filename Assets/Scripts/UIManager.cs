﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class UIManager : MonoBehaviour
{
    public static UIManager curUIManager { get; private set; }

    //private MenuOptions menuOptionsScript;
    public Canvas staticUICancas, dynamicUICanvas, shipSelectionCanvas, MenuCanvas;
    public GameObject creditsPanel;
    CanvasGroup staticUIGroup, dynamicUIGroup;
    float canvasGroupAlpha;
    Transform shipMenuGrid;
    GridLayoutGroup layoutGroup;
    RectTransform _rect;
    //float layoutGridRefCellSize = 120.0f;
    //public Text watchAdToContinueText;
    public TextMeshProUGUI scoreText, ammoText, timeAliveText, scoreMuliplyerText, menuMessage;
    public Slider shieldSlider, oxygenSlider, effectVolumeSlider;
    public Image shieldImage, oxygenImage, ShieldBar, OxygenBar, volumeImage;
    public Sprite volumeImageSprite, volumeImageSprite_Mute;
    float shieldMaxValue = 1.0f;
    //float shieldImageAlpha_min = 0.2f, shieldImageAlpha_max = 1.0f;
    //Color shieldActiveColour = Color.white, shieldInactiveColour = Color.white;
    bool scoreFontSizeSmall = false;

    [Header("Ship Selection Menu")]
    [SerializeField] float cellSizePortrait;
    [SerializeField] float cellSizeLandscape;
    [SerializeField] int columnCountPortrait, colomnCountLandscape;

    void Awake()
    {
        if (curUIManager != null && curUIManager != this)
        {
            Destroy(gameObject);
            return;
        }
        curUIManager = this;
        //shieldActiveColour.a = shieldImageAlpha_max;
        //shieldInactiveColour.a = shieldImageAlpha_min;
        shipMenuGrid = GameObject.FindGameObjectWithTag("ShipMenuGrid").transform;
        layoutGroup = shipMenuGrid.GetComponent<GridLayoutGroup>();
        _rect = shipMenuGrid.GetComponent<RectTransform>();
        shipSelectionCanvas.enabled = false;
        scoreFontSizeSmall = false;
        staticUIGroup = staticUICancas.GetComponent<CanvasGroup>();
        dynamicUIGroup = dynamicUICanvas.GetComponent<CanvasGroup>();
        canvasGroupAlpha = 0.0f;
        staticUIGroup.alpha = canvasGroupAlpha;
        dynamicUIGroup.alpha = canvasGroupAlpha;
        effectVolumeSlider.onValueChanged.RemoveAllListeners(); //just in case
        effectVolumeSlider.onValueChanged.AddListener(delegate { EffectVolumeChange(); });
    }

    void Start()
    {
        float _volVal = AudioManager.audioManager.SoundEffectVolume;
        effectVolumeSlider.value = _volVal;
        volumeImage.sprite = _volVal > 0.0f ? volumeImageSprite : volumeImageSprite_Mute;
        creditsPanel.SetActive(false);
    }

    void EffectVolumeChange()
    {
        AudioManager.audioManager.UpdateSoundEffectVolume(effectVolumeSlider.value);
        volumeImage.sprite = effectVolumeSlider.value > 0.0f ? volumeImageSprite : volumeImageSprite_Mute;
    }

    public void FadeStatsUI(bool fadeOut)
    {
        StopCoroutine("FadeStatsCanvases");
        StartCoroutine("FadeStatsCanvases", fadeOut);
    }

    IEnumerator FadeStatsCanvases(bool fadeOut)
    {
        bool _fade = true;
        while (_fade == true)
        {
            if (fadeOut == true)
            {
                canvasGroupAlpha -= 2.0f * Time.unscaledDeltaTime;
                staticUIGroup.alpha = canvasGroupAlpha;
                dynamicUIGroup.alpha = canvasGroupAlpha;

                if(canvasGroupAlpha <= 0.05f)
                {
                    _fade = false;
                }
            }
            else
            {
                canvasGroupAlpha += 15.0f * Time.unscaledDeltaTime;
                staticUIGroup.alpha = canvasGroupAlpha;
                dynamicUIGroup.alpha = canvasGroupAlpha;

                if (canvasGroupAlpha >= 0.95f)
                {
                    _fade = false;
                }
            }
            
            yield return null;
        }

        canvasGroupAlpha = fadeOut == true ? 0.0f : 1.0f;
        staticUIGroup.alpha = canvasGroupAlpha;
        dynamicUIGroup.alpha = canvasGroupAlpha;
    }

    /// <summary>
    /// Show in-game continue options menu - on player death
    /// </summary>
    /// <param name="show"></param>
    public void DisplayPopUpMenu(bool show, string message = "")
    {
        if (show == true)
        {
            //watchAdToContinueText.text = "Watch a short AD to continue AND get " + UnityAdManager.curAdManager.BonusCrystals.ToString() + " BONUS Crystals"; //Ads no longer used
            float _volVal = AudioManager.audioManager.SoundEffectVolume;
            effectVolumeSlider.value = _volVal;
            volumeImage.sprite = _volVal > 0.0f ? volumeImageSprite : volumeImageSprite_Mute;
            menuMessage.text = message;
            MenuCanvas.enabled = true;
            GameManager.curGameManager.ForceGarbageCollection(true);
        }
        else
        {
            MenuCanvas.enabled = false;
        }
    }

    public void OpenCreditsPanel()
    {
        Time.timeScale = 0.0f;
        creditsPanel.SetActive(true);
    }

    public void ButtonActions(bool selection)
    {
        switch (selection)
        {
            //case "Continue":
            //    GameManager.curGameManager.RespawnPlayer();
            //    popUpMenu.SetActive(false);
            //    //if (GameManager.curGameManager.playerCrystals >= 5)
            //    //{
            //    //    GameManager.curGameManager.UpdatePickups(2, -5);
            //    //    GameManager.curGameManager.RespawnPlayer();
            //    //    popUpMenu.SetActive(false);
            //    //}
            //    break;
            case true:
                //Scene scene = SceneManager.GetActiveScene();
                //SceneManager.LoadScene(scene.name);
                //GameManager.curGameManager.PlayerShipSelectionID = 
                //GameManager.curGameManager.RespawnPlayer();
                GameManager.curGameManager.CurrentGameState = GameState.Play;
                MenuCanvas.enabled = false;
                break;
            //case "WatchAd": //Ads no longer used
            //    popUpMenu.SetActive(false);
            //    UnityAdManager.curAdManager.ShowRewardAd();
            //    //TODO -----remove when ads reactivated
            //    //GameManager.curGameManager.PlayerWatchedAd();
            //    //GameManager.curGameManager.RespawnPlayer();
            //    //-------
            //    break;
            case false:
                //#if UNITY_EDITOR
                //                UnityEditor.EditorApplication.isPlaying = false;
                //#endif
                //                Application.Quit();
                GameManager.curGameManager.CurrentGameState = GameState.StartMenu;
                GameManager.curGameManager.RespawnPlayer();
                MenuCanvas.enabled = false;
                //menuOptionsScript.GoToMenu();
                //TODO - pretty rough - fix this - reset score/crystals on NO continue
                GameManager.curGameManager.playerScore = 0;
                GameManager.curGameManager.playerCrystals = 0;
                //end
                break;

        }
    }

    //UI Text updaters
    public void UpdateScoreText(int val)
    {
        if(scoreFontSizeSmall == true)
        {
            if(val <= 9999999)
            {
                scoreFontSizeSmall = false;
                scoreText.fontSize = 90.0f;
            }
            
        }
        else if(val > 9999999)
        {
            scoreFontSizeSmall = true;
            scoreText.fontSize = 70.0f;
        }

        scoreText.text = val.ToString();
    }

    public void UpdateAmmoText(int val, bool active = false)
    {
        ammoText.text = active == true ? val.ToString() : "inf";
    }

    public void UpdateTimeText(float val)
    {
        timeAliveText.text = Mathf.Floor(val / 60).ToString("00") + ":" + Mathf.Floor(val % 60).ToString("00");
    }

    //public void UpdateCrystalText(int val)
    //{
    //    crystalsText.text = val.ToString();
    //}

    public void UpdateScoreMultiplyerText(int val)
    {
        scoreMuliplyerText.text = "x" + val.ToString();
    }

    //public void SetShieldUiImageAlpha(bool active)
    //{
    //    shieldImage.color = active == true ? shieldActiveColour : shieldInactiveColour;
    //}

    public void UpdateShieldSlider(float val)
    {
        ShieldBar.fillAmount = val / shieldMaxValue;
    }

    public void UpdateShieldMaxValue(float val)
    {
        shieldMaxValue = val;
    }

    public void UpdateOxygenBar(float val)
    {
        OxygenBar.fillAmount = val;
    }

    public void ToggleShipSelectionCanvas(bool enable)
    {
        MenuCanvas.enabled = false;
        //if(shipSelectionCanvas.enabled == false || enable == true) //TODO
        //{
        //    UpdateShipsSelectableDisplay();
        //}
        if (shipSelectionCanvas.enabled == false || enable == true)
        {            
            ResizeLaoutGrid();
        }
        shipSelectionCanvas.enabled = !shipSelectionCanvas.enabled;
        if(enable)
        {
            GameManager.curGameManager.CurrentGameState = GameState.ShipSelection;
        }
        //popUpMenu.enabled = false;
    }

    //public void UpdateShipsSelectableDisplay()
    //{
    //    //change to function on ShipMenuInfo.
    //    foreach (ShipMenuInfo i in shipMenuGrid.GetComponentsInChildren<ShipMenuInfo>())
    //    {
    //        i.gameObject.GetComponent<Image>().color = new Color(1.0f, 1.0f, 1.0f, 0.3f);
    //    }
    //}

    public void AddShipToSelectionMenu(ShipDisplayInformation shipInfo, bool calculateLayout)
    {
        GameObject _menuItem = Instantiate(Resources.Load<GameObject>("ShipMenuItem"), shipMenuGrid);
        _menuItem.name = shipInfo.shipName;
        _menuItem.GetComponent<Image>().preserveAspect = true;
        ShipMenuInfo _info = _menuItem.GetComponent<ShipMenuInfo>();
        _info.DisplayInformation = shipInfo;

        if(calculateLayout == true)
        {
            ResizeLaoutGrid();
        }
    }

    public void ResizeLaoutGrid()
    {
        bool _isLandscape = ScreenOrientationChecker.orientationChecker.CamLandscape;
        float _amt = _isLandscape == true ? cellSizeLandscape : cellSizePortrait;
        layoutGroup.constraintCount = _isLandscape == true ? colomnCountLandscape : columnCountPortrait;

        layoutGroup.cellSize = new Vector2(_amt, _amt);

    }

    public void OpenExternalURL()
    {
        //app privacy policy
        Application.OpenURL("https://drive.google.com/file/d/1Za95YBIuw5HBekJfz0SRsJp54rOfZYPR/view?usp=sharing");
    }

    private void OnDestroy()
    {
        effectVolumeSlider.onValueChanged.RemoveAllListeners();
    }
}
