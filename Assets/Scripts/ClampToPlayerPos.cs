﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Clamp GameObject's position the the Player position
/// </summary>
public class ClampToPlayerPos : MonoBehaviour {

    public GameObject player; 
	
	void Update () {
	
        transform.position = player.transform.position;
	}
}
