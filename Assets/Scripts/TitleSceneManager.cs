﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleSceneManager : MonoBehaviour
{
    //bool animationDone = false;
    

    void Awake()
    {
        //animationDone = false;
        Screen.orientation = ScreenOrientation.Portrait;        
        //StartCoroutine(LoadMainSceneAsync()); // - loadSceneAsync issues in current engine build - switches scene too quickly and allowSceneActivation not working as expected
    }

    //IEnumerator LoadMainSceneAsync()
    //{
    //    AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(1);
    //    asyncOperation.allowSceneActivation = false;


    //    while (!asyncOperation.isDone)
    //    {
    //        if (animationDone)
    //        {
    //            asyncOperation.allowSceneActivation = true;
    //        }
    //        yield return null;
    //    }

    //    //yield return new WaitUntil(()=>animationDone == true);
    //    //asyncOperation.allowSceneActivation = true;
    //    //Debug.Log(animationDone.ToString() + "Progress: " + asyncOperation.progress.ToString());
    //    //yield return new WaitForEndOfFrame();
    //}

    public void SwitchScene()
    {
        SceneManager.LoadScene(1);
        //animationDone = true;
    }
}
