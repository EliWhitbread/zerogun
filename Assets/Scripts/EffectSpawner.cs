﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EffectSpawner : MonoBehaviour {

    public static EffectSpawner curEffectSpawner { get; private set; }

    public GameObject explosionEffect;
    public int explosionsToPool;

    List<GameObject> explosionPool;

    void Awake()
    {
        if(curEffectSpawner != null && curEffectSpawner != this)
        {
            Destroy(gameObject);
            return;
        }
        curEffectSpawner = this;
    }

	void Start () {

        explosionPool = new List<GameObject>();

        for (int i = 0; i < explosionsToPool; i++)
        {
            GameObject obj = (GameObject)Instantiate(explosionEffect);
            obj.SetActive(false);
            explosionPool.Add(obj);
        }
    }
	
    /// <summary>
    /// Spawn an explosion at position
    /// </summary>
    /// <param name="pos"></param>
	public void SpawnExplosion(Vector3 pos)
    {
        for (int i = 0; i < explosionPool.Count; i++)
        {
            if (!explosionPool[i].activeInHierarchy)
            {
                explosionPool[i].transform.position = pos;
                explosionPool[i].transform.rotation = Quaternion.identity;
                explosionPool[i].SetActive(true);
                break;
            }
        }
    }
}
