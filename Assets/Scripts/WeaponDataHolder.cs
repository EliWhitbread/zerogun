﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponDataHolder : MonoBehaviour
{
    public static WeaponDataHolder curWeaponDataHolder { get; private set; }

    public WeaponData baseWeaponData;
    public WeaponData bigShotWeaponData;
    public WeaponData laserWeaponData;
    public WeaponData spreadShotWeaponData;

    private void Awake()
    {
        if(curWeaponDataHolder != null && curWeaponDataHolder != this)
        {
            Destroy(this);
        }
        else
        {
            curWeaponDataHolder = this;
        }
    }

}

[System.Serializable]
public struct WeaponData
{
    public float fireDelay, lifeTime, speed, playerMoveForce;
    public int damageToTarget, maxAmmo;
    public List<float> shootAngles;
}

