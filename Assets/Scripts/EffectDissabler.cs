﻿using UnityEngine;
using System.Collections;

public class EffectDissabler : MonoBehaviour {

    ParticleSystem thisEmitter;

	void Start()
    {
        thisEmitter = GetComponent<ParticleSystem>();
    }

    //disable the gameObject once the attached Particle System has stopped
    void Update()
    {
        if(!thisEmitter.IsAlive())
        {
            gameObject.SetActive(false);
        }
    }
}
