﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Entity : MonoBehaviour {

    [SerializeField] EntityStats itemStats;
    Transform player;

    int curHitPoints;
    float curTime, customTick = 0.2f, entityDisableDistance = 20.0f;


    public EntityStats ItemStats
    {
        get { return itemStats; }
    }

    private void OnEnable()
    {
        curHitPoints = itemStats.hitPoints;
        curTime = 0.0f;
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }

    private void Update()
    {
        curTime += Time.deltaTime;
        //disable Entity if out of range of the Player
        if (player != null && curTime >= customTick)
        {
            curTime = 0.0f;
            Vector2 offset = player.transform.position - transform.position;
            float sqrLength = offset.sqrMagnitude;
            if (sqrLength > entityDisableDistance * entityDisableDistance)
            {
                DeactivateEntity();
            }
        }
    }

    public void TakeDamage(int hitDamage)
    {
        curHitPoints -= hitDamage;
        if (curHitPoints <= 0 && gameObject.activeInHierarchy)
        {
            GameManager.curGameManager.UpdatePlayerScore(itemStats.scoreValue);
            DeactivateObject();
        }
    }

    public void DeactivateObject()
    {
        AudioManager.audioManager.PlayAudio(AudioEffectClip.AsteroidSmash);
        EffectSpawner.curEffectSpawner.SpawnExplosion(transform.position);
        EnemySpawner.curEnemySpawner.SpawnPickupAtAsteroidPos(transform.position);
        //gameObject.GetComponent<CircleCollider2D>().enabled = false;
        DeactivateEntity();
    }

    void DeactivateEntity()
    {
        gameObject.SetActive(false);
    }

}
