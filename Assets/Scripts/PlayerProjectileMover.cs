﻿using UnityEngine;
using System.Collections;

public class PlayerProjectileMover : MonoBehaviour {

    public float moveSpeed, lifeTime, playerSpeedAddition;
    public int damage;
    Transform myTransform;
    //PlayerGunManager gunManager;
    SpriteRenderer myRenderer;
    Animator anim;
    bool canMove = true;

    bool imediate = false;

    private void Start()
    {
       // gunManager = PlayerGunManager.curPlayerGunManager;
        myRenderer = gameObject.GetComponentInChildren<SpriteRenderer>();
    }


    void OnEnable()
    {
        if(myTransform != this.transform)
        {
            myTransform = this.transform;
        }

        //if (gunManager == null)
        //{
        //    gunManager = PlayerGunManager.curPlayerGunManager;
        //}

        if (myRenderer == null)
        {
            myRenderer = gameObject.GetComponentInChildren<SpriteRenderer>();
        }

        if(anim == null)
        {
            anim = gameObject.GetComponentInChildren<Animator>();
        }

        Color tmpCol = myRenderer.color;
        tmpCol.a = 1.0f;
        myRenderer.color = tmpCol;

        //moveSpeed = gunManager.WeaponStats.baseSpeed;
        //damage = gunManager.WeaponStats.baseDamage;
        //lifeTime = gunManager.WeaponStats.timeAlive;

        //moveSpeed = gunManager.CurrentWeaponData.speed;
        //damage = gunManager.CurrentWeaponData.damageToTarget;
        //lifeTime = gunManager.CurrentWeaponData.lifeTime;

        imediate = false;
        canMove = true;
        Invoke("DestroySelf", lifeTime);
    }	

    public void AnimFinished()
    {
        gameObject.SetActive(false);
    }

    void DestroySelf()
    {
        if(imediate == true)
        {
            anim.SetTrigger("Hit");
            return;
        }
        else if(gameObject.activeInHierarchy)
        {
            StartCoroutine("FadeProjectile");
        }
        
    }

	void Update () {

        if (!canMove)
        {
            return;
        }

        myTransform.Translate(Vector2.up * moveSpeed * Time.deltaTime);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        canMove = false;
        CancelInvoke();
        Entity entity = other.GetComponent<Entity>();
        if (entity != null)
        {
            entity.TakeDamage(damage);
        }
        imediate = true;
        CancelInvoke();
        DestroySelf();
    }

    IEnumerator FadeProjectile()
    {
        
        Color c = myRenderer.color;
        float alpha = c.a;

        while (alpha > 0)
        {
            alpha -= 0.5f;
            c.a = alpha;
            myRenderer.color = c;
            yield return null;
        }

        gameObject.SetActive(false);
    }

}
