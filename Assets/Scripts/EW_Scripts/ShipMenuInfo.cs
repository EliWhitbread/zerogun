﻿
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ShipMenuInfo : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private ShipDisplayInformation displayInformation;
    Image menuImage;

    public ShipDisplayInformation DisplayInformation
    {
        get { return displayInformation; }
        set { displayInformation = value; }
    }

    void Start()
    {
        menuImage = gameObject.GetComponent<Image>();
        menuImage.sprite = displayInformation.shipDisplaySprite;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        PersistentData.PlayerShipSelection = displayInformation.shipID;
        GameManager.curGameManager.playerSelectedShip = true;
        GameManager.curGameManager.RespawnPlayer(); //TODO - umm yeah.
        UIManager.curUIManager.ToggleShipSelectionCanvas(false);
        UIManager.curUIManager.DisplayPopUpMenu(false);
        GameManager.curGameManager.CurrentGameState = GameState.Play;
    }

}
