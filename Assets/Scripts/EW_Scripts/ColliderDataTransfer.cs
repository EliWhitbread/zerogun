﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class ColliderDataTransfer : MonoBehaviour
{
    public PolygonCollider2D originDataCollider;
    public PlayerShipScriptableObject target;

#if UNITY_EDITOR
    public void TransferColliderData()
    {
        target.mainShipPolygonCollider = originDataCollider.GetPath(0);

        EditorUtility.SetDirty(target);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
#endif
}
