﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Weapon { Base, Base_Spread, BigShot, BigShot_Spread, Laser, Laser_BigShot, Laser_Spread, Laser_BigShot_Spread }


public class WeaponManager : MonoBehaviour {

    public static WeaponManager curWeaponManager { get; private set; }
    [SerializeField]WeaponScriptableObject currentWeaponData;
    GameObject projectile;
    List<GameObject> projectiles;
    bool canShoot = false;
    float shootClock = 0.0f;
    Transform firePoint;
    [SerializeField]int currentAmmo;
    [SerializeField] ProjectileSpawnPointInit projectileSpawnInit;

    //weapon stats
    float fireDelay, playerMoveForce;
    float[] projectileSpawnRotations;

    [Header("Weapon Data")]
    public WeaponScriptableObject baseWeaponData, baseSpreadData, bigShotData, bigShotSpreadData, laserData, laserBigShotData, laserSpreadData, laserBigShotSpreadData;

    public WeaponScriptableObject CurrentWeaponData
    {
        get { return currentWeaponData; }
        set { currentWeaponData = value; UpdateCurrentWeapon(value.projectile == projectile? false:true); }
    }

    public ProjectileSpawnPointInit ProjectileSpawnInit { set { projectileSpawnInit = value; } }

    public bool CanShoot
    {
        get { return canShoot; }
        set { canShoot = value; SwitchLasersActive(value); }
    }

    public Transform SetFirePoint
    {
        set { firePoint = value; }
    }

    private void Awake()
    {
        if(curWeaponManager != null && curWeaponManager != this)
        {
            Destroy(this);
        }
        else
        {
            curWeaponManager = this;
        }

        //if(currentWeaponData == null)
        //{
        //    currentWeaponData = baseWeaponData;
        //}
        projectiles = new List<GameObject>();
        //CurrentWeaponData = baseWeaponData;
    }

    public void InitWeaponManager()
    {
        CurrentWeaponData = baseWeaponData;

    }

    private void Update()
    {
        if(canShoot == false || GameManager.curGameManager.CurrentGameState != GameState.Play)
        {
            return;
        }

        shootClock += Time.deltaTime;

        if(shootClock >= fireDelay && currentAmmo > 0)
        {
            shootClock = 0.0f;
            switch (currentWeaponData.weapon)
            {
                case Weapon.Base:
                    AudioManager.audioManager.PlayAudio(AudioEffectClip.Projectile_Small);
                    FireProjectile();
                    break;
                case Weapon.Base_Spread:
                    AudioManager.audioManager.PlayAudio(AudioEffectClip.Projectile_Small);
                    FireProjectile();
                    break;
                case Weapon.BigShot:
                    AudioManager.audioManager.PlayAudio(AudioEffectClip.Projectile_Large);
                    FireProjectile();
                    break;
                case Weapon.BigShot_Spread:
                    AudioManager.audioManager.PlayAudio(AudioEffectClip.Projectile_Large);
                    FireProjectile();
                    break;
                case Weapon.Laser:
                    FireLaser();
                    break;
                case Weapon.Laser_BigShot:
                    FireLaser();
                    break;
                case Weapon.Laser_Spread:
                    FireLaser();
                    break;
                case Weapon.Laser_BigShot_Spread:
                    FireLaser();
                    break;
                default:
                    break;
            }
            
        }
        
    }

    void UpdateAmmoOnly(int ammo)
    {
        currentAmmo = ammo;
    }

    public void AddWeaponPickUp(StackableWeapons pickupType)
    {
        bool changeProjectile = false; //keep the current pooled projectile or not
        switch (currentWeaponData.weapon)
        {
            case Weapon.Base:
                switch (pickupType)
                {
                    case StackableWeapons.BIG_SHOT:
                        currentWeaponData = bigShotData;
                        changeProjectile = true;
                        break;
                    case StackableWeapons.LASER:
                        currentWeaponData = laserData;
                        changeProjectile = true;
                        break;
                    case StackableWeapons.SPREAD_SHOT:
                        currentWeaponData = baseSpreadData;
                        break;
                    default:
                        break;
                }
                break;
            case Weapon.Base_Spread:
                switch (pickupType)
                {
                    case StackableWeapons.BIG_SHOT:
                        currentWeaponData = bigShotSpreadData;
                        changeProjectile = true;
                        break;
                    case StackableWeapons.LASER:
                        currentWeaponData = laserSpreadData;
                        changeProjectile = true;
                        break;
                    case StackableWeapons.SPREAD_SHOT:
                        //do nothing - already set
                        break;
                    default:
                        break;
                }
                break;
            case Weapon.BigShot:
                switch (pickupType)
                {
                    case StackableWeapons.BIG_SHOT:
                        //do nothing - already set
                        break;
                    case StackableWeapons.LASER:
                        currentWeaponData = laserBigShotData;
                        changeProjectile = true;
                        break;
                    case StackableWeapons.SPREAD_SHOT:
                        currentWeaponData = bigShotSpreadData;
                        break;
                    default:
                        break;
                }
                break;
            case Weapon.BigShot_Spread:
                switch (pickupType)
                {
                    case StackableWeapons.BIG_SHOT:
                        //do nothing - already set
                        break;
                    case StackableWeapons.LASER:
                        currentWeaponData = laserBigShotSpreadData;
                        changeProjectile = true;
                        break;
                    case StackableWeapons.SPREAD_SHOT:
                        //do nothing - already set
                        break;
                    default:
                        break;
                }
                break;
            case Weapon.Laser:
                switch (pickupType)
                {
                    case StackableWeapons.BIG_SHOT:
                        currentWeaponData = laserBigShotData;
                        changeProjectile = true;
                        break;
                    case StackableWeapons.LASER:
                        //do nothing - already set
                        break;
                    case StackableWeapons.SPREAD_SHOT:
                        currentWeaponData = laserSpreadData;
                        break;
                    default:
                        break;
                }
                break;
            case Weapon.Laser_BigShot:
                switch (pickupType)
                {
                    case StackableWeapons.BIG_SHOT:
                        //do nothing - already set
                        break;
                    case StackableWeapons.LASER:
                        //do nothing - already set
                        break;
                    case StackableWeapons.SPREAD_SHOT:
                        currentWeaponData = laserBigShotSpreadData;
                        break;
                    default:
                        break;
                }
                break;
            case Weapon.Laser_Spread:
                switch (pickupType)
                {
                    case StackableWeapons.BIG_SHOT:
                        currentWeaponData = laserBigShotSpreadData;
                        changeProjectile = true;
                        break;
                    case StackableWeapons.LASER:
                        //do nothing - already set
                        break;
                    case StackableWeapons.SPREAD_SHOT:
                        //do nothing - already set
                        break;
                    default:
                        break;
                }
                break;
            case Weapon.Laser_BigShot_Spread:
                //do nothing - weapon is at max awesome!
                break;
            default:
                break;
        }

        UpdateCurrentWeapon(changeProjectile);
    }

    public void UpdateCurrentWeapon(bool changeProjectile = false)
    {
        
        if(firePoint == null) //move this to ship swapping mechanic when done
        {
            firePoint = PlayerController.curPlayerController.GetProjectileSpawnPoint;
        }
        if(currentWeaponData == null)
        {
            CurrentWeaponData = baseWeaponData;
        }

        if (changeProjectile == true)
        {
            if (projectiles.Count > 0)
            {
                foreach (GameObject p in projectiles)
                {
                    p.SetActive(false);
                    Destroy(p);
                }
                projectiles.Clear();
            }
            projectile = currentWeaponData.projectile;
        }

        UpdateAmmoOnly(currentWeaponData.ammunition);
        projectileSpawnRotations = currentWeaponData.projectileSpawnRotations;
        playerMoveForce = currentWeaponData.shotMovementForce;
        fireDelay = currentWeaponData.fireDelay;
        shootClock = 0.0f;

        switch (currentWeaponData.weapon)
        {
            case Weapon.Laser:
                SetupLasers();
                break;
            case Weapon.Laser_BigShot:
                SetupLasers();
                break;
            case Weapon.Laser_Spread:
                SetupLasers();
                break;
            case Weapon.Laser_BigShot_Spread:
                SetupLasers();
                break;
            default:
                break;
        }

        if (currentWeaponData.weapon == Weapon.Base)
        {
            UIManager.curUIManager.UpdateAmmoText(currentAmmo, false);
            return;
        }
        else
        {
            UIManager.curUIManager.UpdateAmmoText(currentAmmo, true);
        }
    }

    public void FireProjectile()
    {
        foreach(float rot in projectileSpawnRotations)
        {
            GameObject obj = GetProjectile();
            if (obj == null) return;

            obj.transform.position = firePoint.position;
            obj.transform.rotation = firePoint.rotation * Quaternion.Euler(0, 0, rot);
            obj.SetActive(true);
        }
        PlayerController.curPlayerController.UpdateMoveSpeed(playerMoveForce);

        if (currentWeaponData != baseWeaponData)
        {
            currentAmmo--;
            
            UIManager.curUIManager.UpdateAmmoText(currentAmmo, true);

            if (currentAmmo <= 0)
            {
                CurrentWeaponData = baseWeaponData;
            }
        }
        
    }

    void SetupLasers()
    {
        if(projectiles.Count > 0) //lazy but....??
        {
            foreach (GameObject p in projectiles)
            {
                p.SetActive(false);
                Destroy(p);
            }
            projectiles.Clear();
        }

        foreach (float rot in projectileSpawnRotations)
        {
            GameObject obj = (GameObject)Instantiate(projectile);
            if (obj == null) return;

            obj.transform.position = firePoint.position;
            Laser laser = obj.GetComponent<Laser>();
            laser.Anchor = firePoint;
            laser.OffsetAngleEuler = rot;
            laser.InitialiseLaser();
            obj.SetActive(canShoot);
            projectiles.Add(obj);
        }
    }

    void FireLaser()
    {
        for (int i = 0; i < projectiles.Count; i++)
        {
            projectiles[i].SetActive(true);

            LaserDamage ld = projectiles[i].GetComponentInChildren<LaserDamage>();
            if(ld != null)
            {
                ld.ApplyDamage();
            }
        }
        projectileSpawnInit.ToggleLaserOriginEffect(true, true);
        PlayerController.curPlayerController.UpdateMoveSpeed(playerMoveForce);

        if (currentWeaponData != baseWeaponData)
        {
            currentAmmo--;
            
            UIManager.curUIManager.UpdateAmmoText(currentAmmo, true);

            if (currentAmmo <= 0)
            {
                CurrentWeaponData = baseWeaponData;
                AudioManager.audioManager.PlayLaserAudio(false);
            }
        }
    }

    void SwitchLasersActive(bool activate)
    {

        bool isLaser = false;
        bool isBaseLaser = true;

        switch (currentWeaponData.weapon)
        {
            case Weapon.Laser:
                isLaser = true;
                break;
            case Weapon.Laser_BigShot:
                isLaser = true;
                isBaseLaser = false;
                break;
            case Weapon.Laser_Spread:
                isLaser = true;
                break;
            case Weapon.Laser_BigShot_Spread:
                isLaser = true;
                isBaseLaser = false;
                break;
            default:
                break;
        }

        if(isLaser == true)
        {
            AudioManager.audioManager.PlayLaserAudio(activate);
            for (int i = 0; i < projectiles.Count; i++)
            {
                projectiles[i].SetActive(activate);
                
            }
            projectileSpawnInit.ToggleLaserOriginEffect(activate, isLaser);
        }
        else
        {
            projectileSpawnInit.ToggleLaserOriginEffect(false, isBaseLaser);
        }
        
    }

    GameObject GetProjectile()
    {
        for (int i = 0; i < projectiles.Count; i++)
        {
            if (!projectiles[i].activeInHierarchy)
            {
                return projectiles[i];
            }
        }
        GameObject p = (GameObject)Instantiate(projectile);
        p.SetActive(false);
        projectiles.Add(p);
        return p;
    }
}
