﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour {

    Quaternion rotationOffset = Quaternion.identity;
    [SerializeField] float offsetAngleEuler;
    [SerializeField] int damage;
    Transform laserTransform, myTransform;

    //test
    public Transform anchor;

    public float OffsetAngleEuler
    {
        set { offsetAngleEuler = value; }
    }

    public int Damage
    {
        get { return damage; }
        set { damage = value; }
    }

    public Transform Anchor
    {
        set { anchor = value; }
    }

    //private void OnEnable()
    //{
    //    myTransform = this.transform;
    //    laserTransform = transform.GetChild(0);
    //    SetRotationOffset();
    //}

    public void InitialiseLaser()
    {
        myTransform = this.transform;
        laserTransform = transform.GetChild(0);
        SetRotationOffset();
    }

    private void OnDisable()
    {
        offsetAngleEuler = 0.0f;
    }

    void SetRotationOffset()
    {
        rotationOffset = Quaternion.Euler(0, 0, offsetAngleEuler);
        laserTransform.localRotation = rotationOffset;
    }

    void LateUpdate () {

        myTransform.position = anchor.position;
        myTransform.rotation = anchor.rotation;
    }
    
}
