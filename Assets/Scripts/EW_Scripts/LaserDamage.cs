﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserDamage : MonoBehaviour {

    Laser _laser;

    List<Entity> enemies;

    PolygonCollider2D laserCollider;
    SpriteRenderer spriteRenderer;
    Transform parentTransform;
    GameObject hitEffect;
    [SerializeField] Vector2[] colliderPoints;

    void OnEnable()
    {
        if(_laser == null)
        {
            _laser = GetComponentInParent<Laser>();
        }
        if(laserCollider == null)
        {
            laserCollider = gameObject.GetComponent<PolygonCollider2D>();
            colliderPoints = laserCollider.points;
        }
        if(parentTransform == null)
        {
            parentTransform = gameObject.GetComponentInParent<Transform>();
        }
        if(spriteRenderer == null)
        {
            spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        }
        if(hitEffect == null)
        {
            hitEffect = gameObject.transform.GetChild(0).gameObject;
        }
        hitEffect.SetActive(false);
        if(enemies == null)
        {
            enemies = new List<Entity>();
        }
        else
        {
            enemies.Clear();
        }
        
    }

    private void Update()
    {
        if(enemies.Count > 0)
        {
            float distToOther = Vector2.Distance(parentTransform.position, enemies[0].transform.position);
            colliderPoints[0].y = distToOther;
            colliderPoints[1].y = distToOther;
            laserCollider.points = colliderPoints;
            spriteRenderer.size = new Vector2(0.32f, distToOther);
            Vector3 _pos = hitEffect.transform.localPosition;
            _pos.y = distToOther;
            hitEffect.transform.localPosition = _pos;

            if(!hitEffect.activeSelf)
            {
                hitEffect.SetActive(true);
            }
        }
        else
        {
            if(hitEffect.activeSelf)
            {
                hitEffect.SetActive(false);
            }
        }
    }

    public void ApplyDamage()
    {
        for (int i = 0; i < enemies.Count; i++)
        {
            if (enemies[i].gameObject.activeInHierarchy)
            {
                enemies[i].TakeDamage(_laser.Damage);
            }
        }
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        
        if (other.gameObject.GetComponent<Renderer>().isVisible == false) //break out if enemy is off screen?
        {
            return;
        }
        Entity entity = other.GetComponent<Entity>();
        if (entity != null)
        {
            enemies.Add(entity);
            entity.TakeDamage(_laser.Damage / 5);
        }        
        //hitEffect.SetActive(true);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Entity e = collision.gameObject.GetComponent<Entity>();

        for (int i = 0; i < enemies.Count; i++)
        {
            if (enemies[i] == e)
            {
                enemies.RemoveAt(i);
            }
        }

        colliderPoints[0].y = 9.0f;
        colliderPoints[1].y = 9.0f;
        laserCollider.points = colliderPoints;
        spriteRenderer.size = new Vector2(0.32f, 9.0f);
        //hitEffect.SetActive(false);
    }
    
}
