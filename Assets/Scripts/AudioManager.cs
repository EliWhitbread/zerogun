﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AudioManager : MonoBehaviour
{
    public static AudioManager audioManager { get; private set; }

    AudioClip a_PlayerDeath, a_AsteroidDeath, a_laser, a_Projectile_Small, a_Projectile_Large;
    AudioSource audioSource, audioSource_Laser;
    [Range(0.0f, 1.0f)]float soundEffectVolume = 0.6f;

    public float SoundEffectVolume { get { return soundEffectVolume; } }

    private void Awake()
    {
        if(audioManager != null && audioManager != this)
        {
            Destroy(this);
        }
        else
        {
            audioManager = this;
        }

    }

    private void OnEnable()
    {
        soundEffectVolume = PlayerPrefs.GetFloat("AudVal", 0.6f);
    }

    // Start is called before the first frame update
    void Start()
    {
        if(gameObject.GetComponents<AudioSource>().Length > 0)
        {
            foreach (AudioSource _as in gameObject.GetComponents<AudioSource>())
            {
                Destroy(_as);
            }
        }

        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource_Laser = gameObject.AddComponent<AudioSource>();        

        a_PlayerDeath = Resources.Load<AudioClip>("Audio/PlayerExplosion");
        a_AsteroidDeath = Resources.Load<AudioClip>("Audio/AsteroidSmash");
        a_laser = Resources.Load<AudioClip>("Audio/Laser");
        a_Projectile_Small = Resources.Load<AudioClip>("Audio/ProjectileSmall");
        a_Projectile_Large = Resources.Load<AudioClip>("Audio/ProjectileLarge");

        audioSource_Laser.clip = a_laser;
        audioSource_Laser.loop = true;
        audioSource_Laser.Stop();

    }

    public void PlayAudio(AudioEffectClip audioEffect)
    {
        if (soundEffectVolume <= 0.01f) { return; }
        switch (audioEffect)
        {
            case AudioEffectClip.AsteroidSmash:
                audioSource.pitch = Random.Range(0.80f, 1.05f);
                audioSource.PlayOneShot(a_AsteroidDeath, soundEffectVolume);
                break;
            //case AudioEffectClip.Laser:
            //    PlayLaserAudio(true);
                //break;
            case AudioEffectClip.PlayerDeath:                
                audioSource.PlayOneShot(a_PlayerDeath, soundEffectVolume);
                break;
            case AudioEffectClip.Projectile_Small:
                audioSource.pitch = Random.Range(0.95f, 1.05f);
                audioSource.PlayOneShot(a_Projectile_Small, soundEffectVolume);
                break;
            case AudioEffectClip.Projectile_Large:
                audioSource.pitch = Random.Range(0.97f, 1.05f);
                audioSource.PlayOneShot(a_Projectile_Large, soundEffectVolume);
                break;
            default:
                break;
        }
    }

    public void PlayLaserAudio(bool playClip)
    {
        if(playClip)
        {
            if (soundEffectVolume <= 0.01f) { return; }
            audioSource_Laser.volume = soundEffectVolume;
            audioSource_Laser.Play();
        }
        else
        {
            audioSource_Laser.Stop();
        }
    }

    public void UpdateSoundEffectVolume(float val = 0.8f)
    {
        soundEffectVolume = val;
        PlayerPrefs.SetFloat("AudVal", val);
        PlayerPrefs.Save();

    }
}
