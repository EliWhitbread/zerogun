﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class RandomSpace : MonoBehaviour
//{
//    public static RandomSpace curRandomSpace;

//    // Arrays to hold textures
//    [SerializeField]
//    Texture[] space00;
//    [SerializeField]
//    Texture[] space01;

//    int randomArray;
//    int randomTexture;

//    // Variables to hold the random textures
//    [SerializeField]
//    Texture randomTextureOne;
//    [SerializeField]
//    Texture randomTextureTwo;

//    // GameObjects that hold the materials to swap the textures on
//    [SerializeField]
//    GameObject spaceOne;
//    [SerializeField]
//    GameObject spaceTwo;

//    void Awake ()
//	{
//        if(curRandomSpace != null && curRandomSpace != this)
//        {
//            Destroy(this);
//        }
//        else
//        {
//            curRandomSpace = this;
//        }
        
//        spaceOne = GameObject.Find("Space_00");
//        spaceTwo = GameObject.Find("Space_01");
//    }

//    public void RandomiseSpace ()
//    {
//        GetRandomTexture(randomTextureOne, "one");
//        GetRandomTexture(randomTextureTwo, "two");

//        spaceOne.GetComponent<Renderer>().material.mainTexture = randomTextureOne;
//        spaceTwo.GetComponent<Renderer>().material.mainTexture = randomTextureTwo;
//    }

//    void GetRandomTexture (Texture textureIn, string whichTexture)
//    {
//        if (whichTexture == "two")
//        {
//            randomTexture = Random.Range(0, 16);

//            switch (randomTexture)
//            {
//                case 0:
//                    textureIn = space00[0];
//                    randomTextureTwo = textureIn;
//                    break;
//                case 1:
//                    textureIn = space00[1];
//                    randomTextureTwo = textureIn;
//                    break;
//                case 2:
//                    textureIn = space00[2];
//                    randomTextureTwo = textureIn;
//                    break;
//                case 3:
//                    textureIn = space00[3];
//                    randomTextureTwo = textureIn;
//                    break;
//                case 4:
//                    textureIn = space00[4];
//                    randomTextureTwo = textureIn;
//                    break;
//                case 5:
//                    textureIn = space00[5];
//                    randomTextureTwo = textureIn;
//                    break;
//                case 6:
//                    textureIn = space00[6];
//                    randomTextureTwo = textureIn;
//                    break;
//                case 7:
//                    textureIn = space00[7];
//                    randomTextureTwo = textureIn;
//                    break;
//                case 8:
//                    textureIn = space00[8];
//                    randomTextureTwo = textureIn;
//                    break;
//                case 9:
//                    textureIn = space00[9];
//                    randomTextureTwo = textureIn;
//                    break;
//                case 10:
//                    textureIn = space00[10];
//                    randomTextureTwo = textureIn;
//                    break;
//                case 11:
//                    textureIn = space00[11];
//                    randomTextureTwo = textureIn;
//                    break;
//                case 12:
//                    textureIn = space00[12];
//                    randomTextureTwo = textureIn;
//                    break;
//                case 13:
//                    textureIn = space00[13];
//                    randomTextureTwo = textureIn;
//                    break;
//                case 14:
//                    textureIn = space00[14];
//                    randomTextureTwo = textureIn;
//                    break;
//                case 15:
//                    textureIn = space00[15];
//                    randomTextureTwo = textureIn;
//                    break;
//            }
//        }
//        else
//        {
//            randomTexture = Random.Range(0, 16);

//            switch (randomTexture)
//            {
//                case 0:
//                    textureIn = space01[0];
//                    randomTextureOne = textureIn;
//                    break;
//                case 1:
//                    textureIn = space01[1];
//                    randomTextureOne = textureIn;
//                    break;
//                case 2:
//                    textureIn = space01[2];
//                    randomTextureOne = textureIn;
//                    break;
//                case 3:
//                    textureIn = space01[3];
//                    randomTextureOne = textureIn;
//                    break;
//                case 4:
//                    textureIn = space01[4];
//                    randomTextureOne = textureIn;
//                    break;
//                case 5:
//                    textureIn = space01[5];
//                    randomTextureOne = textureIn;
//                    break;
//                case 6:
//                    textureIn = space01[6];
//                    randomTextureOne = textureIn;
//                    break;
//                case 7:
//                    textureIn = space01[7];
//                    randomTextureOne = textureIn;
//                    break;
//                case 8:
//                    textureIn = space01[8];
//                    randomTextureOne = textureIn;
//                    break;
//                case 9:
//                    textureIn = space01[9];
//                    randomTextureOne = textureIn;
//                    break;
//                case 10:
//                    textureIn = space01[10];
//                    randomTextureOne = textureIn;
//                    break;
//                case 11:
//                    textureIn = space01[11];
//                    randomTextureOne = textureIn;
//                    break;
//                case 12:
//                    textureIn = space01[12];
//                    randomTextureOne = textureIn;
//                    break;
//                case 13:
//                    textureIn = space01[13];
//                    randomTextureOne = textureIn;
//                    break;
//                case 14:
//                    textureIn = space01[14];
//                    randomTextureOne = textureIn;
//                    break;
//                case 15:
//                    textureIn = space01[15];
//                    randomTextureOne = textureIn;
//                    break;
//            }
//        }
//    }
//}
