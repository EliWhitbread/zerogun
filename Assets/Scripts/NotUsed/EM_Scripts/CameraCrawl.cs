﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCrawl : MonoBehaviour
{
    public float movementSpeed;
    private float actualSpeed;
    private GameObject player;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        actualSpeed = movementSpeed / 100;
        this.transform.position = new Vector3(transform.position.x, transform.position.y + actualSpeed, transform.position.z);
        player.transform.position = new Vector3(player.transform.position.x, player.transform.position.y + actualSpeed, player.transform.position.z);
    }
}
