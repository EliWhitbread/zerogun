﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

// Ensure Canvas has a CanvasGroup
[RequireComponent(typeof(CanvasGroup))]
public class MenuOptions : MonoBehaviour
{
    #region Base Menu Options

    public enum MenuState
    {
        Base,
        InGame,
        Missions,
        Leaderboard,
        Settings,
        ShipSelect
    };

    public MenuState currentState;

    [Header("Base Menu Options")]
    public CanvasGroup myCanvasGroup;

    public GameObject play;

    public float playFade;

    public GameObject ships, options, optionsMenu;

    public float optionsFadeTime = 0;

    //public GameObject hamburger;

    public GameObject uiCanvas;

    public Sprite optionsButtonSprite;

    public Sprite returnButtonSprite;

    private bool optionsShowing = false;

    #endregion

    #region Sub Menu Options
    [Header("Sub Menu Options")]
    public GameObject leaders;

    public GameObject settings, mission;

    private GameObject[] subMenuOptions;

    private Vector3 originalSubMenuButtonPosition;

    private KeyValuePair<bool, GameObject> currentSubOptionActive;

    #endregion

    #region Missions Menu Options
    [Header("Mission Menu Options")]
    public GameObject missionPanel;

    public GameObject missionPrefab;

    private GameObject[] currentMissions = new GameObject[3];

    #endregion

    #region Leaderboard Options
    [Header("Leaderboard Options")]
    public LeaderboardManager leaderManager;

    public GameObject leaderboardPanel;

    public GameObject leaderboardUserPrefab;

    public int initialUserPoolSize = 50;

    public List<string> currentUsers = new List<string>();

    #endregion

    // Use this for initialization
    void Start()
    {
        currentState = MenuState.Base;

        myCanvasGroup = this.GetComponent<CanvasGroup>();

//#if UNITY_EDITOR
//        if (CheckDependencies() == false)
//        {
//            EditorApplication.ExecuteMenuItem("Edit/Play");
//        }
//#endif
        PopulateSubMenuOptionsArray();

        PopulateMissions();
    }

    // Update is called once per frame
    void Update()
    {

    }

    #region Check Dependencies

    private bool CheckDependencies()
    {
        System.Diagnostics.Stopwatch sW = new System.Diagnostics.Stopwatch();
        sW.Reset();
        sW.Start();
        bool componentMissing = false, componentChecked = false;

        // Ensure a Canvas Group Has Been Set. If not, set one.
        if (null == myCanvasGroup)
        {
            componentChecked = true;
            if ((myCanvasGroup = this.GetComponent<CanvasGroup>()))
            {
                if (!(myCanvasGroup = this.gameObject.AddComponent<CanvasGroup>()))
                {
                    // Failed to find or add component
                    componentMissing = true;
                }
            }
        }

        if (null == play)
        {
            componentChecked = true;
            play = SetMissingComponent("PlayButton");

            if (null == play)
            {
                // Setting Component Failed
                componentMissing = true;
            }
        }

        if (null == ships)
        {
            componentChecked = true;
            ships = SetMissingComponent("ShipsButton");

            if (null == ships)
            {
                // Setting Component Failed
                componentMissing = true;
            }
        }

        if (null == options)
        {
            componentChecked = true;
            options = SetMissingComponent("OptionsButton");

            if (null == options)
            {
                // Setting Component Failed
                componentMissing = true;
            }
        }

        if (null == optionsMenu)
        {
            componentChecked = true;
            optionsMenu = SetMissingComponent("OptionsMenu");

            if (null == optionsMenu)
            {
                // Setting Component Failed
                componentMissing = true;
            }
        }

        if (null == leaders)
        {
            componentChecked = true;
            leaders = SetMissingComponent("LeaderButton");

            if (null == leaders)
            {
                // Setting Component Failed
                componentMissing = true;
            }
        }

        if (null == settings)
        {
            componentChecked = true;
            settings = SetMissingComponent("SettingsButton");

            if (null == settings)
            {
                // Setting Component Failed
                componentMissing = true;
            }
        }

        if (null == mission)
        {
            componentChecked = true;
            mission = SetMissingComponent("MissionButton");

            if (null == mission)
            {
                // Setting Component Failed
                componentMissing = true;
            }
        }

        if (null == optionsButtonSprite)
        {
            componentChecked = true;

            if (null == optionsButtonSprite)
            {
                Debug.Log("Missing Optons Button Sprite." + System.Environment.NewLine + "Please find this at: Assets/Textures/Placeholders/zg_Options." + System.Environment.NewLine + "Then drag into Menu Options Script.");
                componentMissing = true;
            }
        }

        if (null == returnButtonSprite)
        {
            componentChecked = true;

            if (null == returnButtonSprite)
            {
                Debug.Log("Missing Optons Button Sprite." + System.Environment.NewLine + "Please find this at: Assets/Textures/Placeholders/zg_Return." + System.Environment.NewLine + "Then drag into Menu Options Script.");
                componentMissing = true;
            }
        }

        if (null == missionPanel)
        {
            componentChecked = true;
            componentMissing = true;
        }

        if (null == leaderboardPanel)
        {
            componentChecked = true;
            componentMissing = true;
        }
        else
        {
            if (leaderboardPanel.GetComponent<LeaderboardManager>() == true)
            {
                leaderManager = leaderboardPanel.GetComponent<LeaderboardManager>();
            }
            else
            {
                leaderManager = leaderboardPanel.AddComponent<LeaderboardManager>();

                if (leaderboardPanel.GetComponent<LeaderboardManager>() == false)
                {
                    componentChecked = true;
                    componentMissing = true;
                }
            }
        }

        if (null == leaderboardUserPrefab)
        {
            componentChecked = true;
            componentChecked = true;
        }

        sW.Stop();
        string fixedIssue = componentMissing == true ? "unsuccessfully check" : "fix";
        string message = componentChecked == true ? ("Time taken to " + fixedIssue + " StartMenu dependencies: " + sW.Elapsed) : ("No StartMenu dependencies missing!");
        Debug.Log(message);

        if (componentMissing == true)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    //private Sprite FetchSpriteFromResources(string location)
    //{
    //    Sprite newSprite = AssetDatabase.LoadAssetAtPath<Sprite>(location);

    //    return newSprite;
    //}

    private GameObject SetMissingComponent(string objectName)
    {
        GameObject findObject;

        if (!string.IsNullOrEmpty(objectName))
        {
            Debug.Log("Missing GameObject: " + objectName + ", within Menu Options, located on the StartMenu Canvas Object");

            findObject = CheckIfSubMenuObject(objectName);

            if (findObject == null)
            {
                // Not a Sub Menu Object
                if (!(findObject = GameObject.Find(objectName)))
                {
                    return null;
                }
            }
        }
        else
        {
            // No string provided, return null.
            return null;
        }

        Debug.Log("GameObject: " + objectName + ", within Menu Options, successfully found.");
        return findObject;
    }

    private GameObject CheckIfSubMenuObject(string objectName)
    {
        int count = 0;

        if (objectName == "LeaderButton" || objectName == "SettingsButton" || objectName == "MissionButton")
        {
            for (int i = 0; i < optionsMenu.transform.childCount; i++)
            {
                optionsMenu.transform.GetChild(i).gameObject.SetActive(true);

                if (i == optionsMenu.transform.childCount - 1)
                {
                    for (int j = 0; j < optionsMenu.transform.childCount; j++)
                    {
                        optionsMenu.transform.GetChild(j).gameObject.SetActive(false);
                    }
                }
            }

            switch (objectName)
            {
                case "LeaderButton":
                    count = 0;
                    break;
                case "SettingsButton":
                    count = 1;
                    break;
                case "MissionButton":
                    count = 2;
                    break;
                default:
                    break;
            }
        }
        else
        {
            return null;
        }

        Debug.Log("GameObject: " + objectName + " is a sub menu object.");

        return optionsMenu.transform.GetChild(count).gameObject;
    }

    #endregion

    #region Main Menu Buttons

    /// <summary>
    /// Method for Starting the Game from the Menu
    /// </summary>
    public void StartGame()
    {
        CloseSubMenus();

        if (optionsShowing == true)
        {
            OptionsButton();
        }

        StartCoroutine("FadeBaseMenu");
        currentState = MenuState.InGame;
    }

    /// <summary>
    /// Method for Navigating to the Menu, from the Game
    /// </summary>
    public void GoToMenu()
    {
        StartCoroutine("FadeBaseMenu");
        currentState = MenuState.Base;
    }

    /// <summary>
    /// Closes all sub-menus
    /// </summary>
    private void CloseSubMenus()
    {
        switch (currentState)
        {
            case MenuState.Missions:
                MissionButton();
                break;
            case MenuState.Leaderboard:
                LeaderButton();
                break;
            case MenuState.Settings:
                SettingsButton();
                break;
            case MenuState.ShipSelect:
                ShipButton();
                break;
        }
    }

    /// <summary>
    /// Private Method used by the Play Button, to Play the Game. 
    /// </summary>
    private void PlayButton()
    {
        StartGame();
    }
    //(the PlayButton method may be redundant since the introduction of the StartGame method. 
    //Leaving in for now, just in case later these methods need to be separate, otherwise will join them).

    private IEnumerator FadeBaseMenu()
    {
        float _elapsedTime = 0f;

        float _from = myCanvasGroup.alpha;

        float _to = _from == 0 ? 1 : 0;

        while (_elapsedTime <= playFade)
        {
            _elapsedTime += Time.deltaTime;
            myCanvasGroup.alpha = Mathf.Lerp(_from, _to, _elapsedTime / playFade);
            yield return null;
        }

        myCanvasGroup.alpha = _to;
        if (_to == 0)
        {
            // Start Game
            this.transform.GetComponent<CanvasGroup>().blocksRaycasts = false;
            uiCanvas.GetComponent<CanvasGroup>().alpha = 1;
            //hamburger.SetActive(true);
            GameManager.curGameManager.CurrentGameState = GameState.Play;
        }
        else if (_to == 1)
        {
            // Go to Menu
            //hamburger.gameObject.SetActive(false);
            uiCanvas.GetComponent<CanvasGroup>().alpha = 0;
            //uiCanvas.GetComponent<UIManager>().popUpMenu.SetActive(true);
            GameManager.curGameManager.CurrentGameState = GameState.StartMenu;
            this.transform.GetComponent<CanvasGroup>().blocksRaycasts = true;
        }
    }

    public void ShipButton()
    {
        if (currentState != MenuState.InGame)
        {
            if (currentState != MenuState.ShipSelect)
            {
                currentState = MenuState.ShipSelect;
            }
            else
            {
                currentState = MenuState.Base;
            }
        }
    }

    public void OptionsButton()
    {
        if (currentState != MenuState.InGame)
        {
            optionsShowing = optionsShowing == false ? true : false;

            options.GetComponent<Image>().sprite = optionsShowing == false ? optionsButtonSprite : returnButtonSprite;

            StartCoroutine("ShowOptions");
        }
    }

    private IEnumerator ShowOptions()
    {
        GameObject toChange1 = optionsShowing == true ? mission : leaders;
        GameObject toChange2 = optionsShowing == true ? leaders : mission;

        toChange1.SetActive(optionsShowing);
        yield return new WaitForSeconds(optionsFadeTime);
        settings.SetActive(optionsShowing);
        yield return new WaitForSeconds(optionsFadeTime);
        toChange2.SetActive(optionsShowing);
    }
    #endregion

    #region Options Sub-Menu

    private void PopulateSubMenuOptionsArray()
    {
        subMenuOptions = new GameObject[] { mission, settings, leaders };
    }

    public void LeaderButton()
    {
        if (currentState != MenuState.InGame)
        {
            if (currentSubOptionActive.Key == false)
            {
                FocusSubMenuButton(leaders);
                currentState = MenuState.Leaderboard;
            }
            else
            {
                UnfocusCurrentSubMenuButton(leaders);
            }

            leaderboardPanel.SetActive(!leaderboardPanel.activeInHierarchy);
            if (leaderboardPanel.activeInHierarchy == true)
            {
                PopulateLeaderboard();
            }
            else
            {
                leaderManager.RemoveLeaderboard();
            }
        }
    }

    public void SettingsButton()
    {
        if (currentState != MenuState.InGame)
        {
            if (currentSubOptionActive.Key == false)
            {
                FocusSubMenuButton(settings);
                currentState = MenuState.Settings;
            }
            else
            {
                UnfocusCurrentSubMenuButton(settings);
            }
        }
    }

    public void MissionButton()
    {
        if (currentState != MenuState.InGame)
        {
            if (currentSubOptionActive.Key == false)
            {
                FocusSubMenuButton(mission);
                currentState = MenuState.Missions;
            }
            else
            {
                UnfocusCurrentSubMenuButton(mission);
            }

            missionPanel.SetActive(!missionPanel.activeInHierarchy);
        }
    }

    public void FocusSubMenuButton(GameObject buttonToFocus)
    {
        currentSubOptionActive = new KeyValuePair<bool, GameObject>(true, buttonToFocus);

        for (int i = 0; i < subMenuOptions.Length; i++)
        {
            if (subMenuOptions[i] != buttonToFocus)
            {
                subMenuOptions[i].gameObject.SetActive(false);
            }
        }

        options.gameObject.SetActive(false);

        originalSubMenuButtonPosition = buttonToFocus.transform.position;
        buttonToFocus.transform.position = new Vector3(options.transform.position.x, options.transform.position.y, options.transform.position.z + 1);

    }

    public void UnfocusCurrentSubMenuButton(GameObject buttonToUnfocus)
    {
        buttonToUnfocus.transform.position = originalSubMenuButtonPosition;

        for (int i = 0; i < subMenuOptions.Length; i++)
        {
            subMenuOptions[i].gameObject.SetActive(true);
        }

        options.gameObject.SetActive(true);

        currentSubOptionActive = new KeyValuePair<bool, GameObject>(false, buttonToUnfocus);
        currentState = MenuState.Base;
    }

    #endregion

    #region Main Game Buttons

    public void HamburgerButton()
    {
        GoToMenu();
    }

    #endregion

    #region Missions Menu

    private void PopulateMissions()
    {
        for (int i = 0; i < currentMissions.Length; i++)
        {
            currentMissions[i] = Instantiate(missionPrefab);

            currentMissions[i].transform.GetChild(0).GetComponent<Text>().text = string.Format("{0}0c", i);
            currentMissions[i].transform.GetChild(1).GetComponent<Text>().text = "Destroy enemy ships";
            currentMissions[i].transform.GetChild(2).GetComponent<Text>().text = string.Format("{0}0/100", i);
            currentMissions[i].transform.GetChild(3).GetComponent<Slider>().value = i * 0.1f;
            int tempHold = i;
            currentMissions[i].transform.GetChild(4).GetComponent<Button>().onClick.AddListener(() => RemoveMission(tempHold));

            currentMissions[i].transform.SetParent(missionPanel.transform.GetChild(1));
            currentMissions[i].transform.localScale = new Vector3(1, 1, 1);
        }
    }

    public void RemoveMission(int button)
    {
        currentMissions[button].transform.GetChild(0).GetComponent<Text>().text = "0c";
        currentMissions[button].transform.GetChild(1).GetComponent<Text>().text = "Cleared";
        currentMissions[button].transform.GetChild(2).GetComponent<Text>().text = "100/100";
        currentMissions[button].transform.GetChild(3).GetComponent<Slider>().value = 1;
    }

    #endregion

    #region Leaderboard Menu

    private void PopulateLeaderboard()
    {
        //leaderManager.userPrefab = this.leaderboardUserPrefab;

        //-- Start testing (Used for testing)
        leaderManager.AddUser("elliot", 20);
        leaderManager.AddUser("eli", 50);
        leaderManager.AddUser("erin", 65);
        leaderManager.AddUser("troy", 30);
        leaderManager.AddUser("james", 10);
        leaderManager.AddUser("tori", 40);
        leaderManager.AddUser("lachlan");
        //-- End testing

        leaderManager.CreateLeaderboard();
    }

    #endregion

}
