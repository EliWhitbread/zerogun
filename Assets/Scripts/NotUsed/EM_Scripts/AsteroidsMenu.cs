﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidsMenu : MonoBehaviour
{
    public GameObject asteroid, player, spawnPosTestObj, asteroidParent;
    public int asteroidsToPool, initialAsteroidPlacementCount;
    public float asteroidSpawnInterval, curAsteroidSpawnInterval, 
        initialDistToPlayerBuffer, maxSpawnDistX, maxSpawnDistY, screenHeight, screenWidth;

    float myTime;

    List<GameObject> asteroidPool = new List<GameObject>();

    // Use this for initialization
    void Start()
    {
        FillAsteroidPool();
        CalculateCameraBounds();
        SpawnTimeUpdate(2);
    }

    private void FillAsteroidPool()
    {
        for (int i = 0; i < asteroidsToPool; i++)
        {
            GameObject obj = Instantiate(asteroid) as GameObject;
            obj.SetActive(false);
            obj.transform.SetParent(asteroidParent.transform);
            asteroidPool.Add(obj);
        }

        InitialAsteroidSpawn();
    }

    void CalculateCameraBounds()
    {
        screenHeight = Camera.main.orthographicSize;
        screenWidth = screenHeight * Camera.main.aspect;
    }

    void InitialAsteroidSpawn()
    {
        Vector2 pos = Vector2.zero;

        for (int i = 0; i < initialAsteroidPlacementCount; i++)
        {
            if (GetInitialPlacementPosition(out pos))
            {
                if (!asteroidPool[i].activeInHierarchy)
                {
                    asteroidPool[i].transform.position = pos;
                    asteroidPool[i].transform.rotation = Quaternion.identity;
                    asteroidPool[i].SetActive(true);
                }
            }
            else
            {
                i--;
            }
        }

    }

    bool GetInitialPlacementPosition(out Vector2 spawnPos)
    {
        float xPos = Random.Range(player.transform.position.x - 5.0f, player.transform.position.x + 5.0f);
        float yPos = Random.Range(player.transform.position.y - 5.0f, player.transform.position.y + 5.0f);

        Vector3 pos = new Vector3(xPos, yPos, 0);

        Vector2 offset = player.transform.position - pos;
        float sqrLength = offset.sqrMagnitude;

        if (sqrLength < initialDistToPlayerBuffer * initialDistToPlayerBuffer)
        {
            spawnPos = Vector2.zero;
            return false;
        }

        spawnPos = pos;
        return true;
    }

    // Update is called once per frame
    void Update()
    {
        float t = Time.deltaTime;
        myTime += t;

        if (myTime >= curAsteroidSpawnInterval)
        {
            SpawnAsteroid();
        }
    }

    public void SpawnTimeUpdate(float value)
    {
        curAsteroidSpawnInterval = Mathf.Clamp((asteroidSpawnInterval / (value * 10)), 0.05f, 10.0f);
    }

    void SpawnAsteroid()
    {
        Vector2 spawnPos = Vector2.zero;
        if (FindSpawnPosition(out spawnPos))
        {
            myTime = 0;

            for (int i = 0; i < asteroidPool.Count; i++)
            {

                if (!asteroidPool[i].activeInHierarchy)
                {
                    asteroidPool[i].transform.position = spawnPos;
                    asteroidPool[i].transform.rotation = Quaternion.identity;
                    asteroidPool[i].SetActive(true);
                    break;
                }

            }
        }
    }
    
    bool FindSpawnPosition(out Vector2 spnPos)
    {
        float xPos, yPos;
        Vector2 newPos = Vector2.zero;

        // 0 = up, 1 = down, 2 = left, 3 = right, default = up
        int randomDirChoice = Random.Range(0, 6);

        switch (randomDirChoice)
        {
            case 0:
                xPos = Random.Range(-screenWidth, screenWidth);
                yPos = Random.Range(1, maxSpawnDistY);
                newPos = new Vector2(player.transform.position.x + xPos, player.transform.position.y + screenHeight + yPos);
                break;
            case 1:
                xPos = Random.Range(-screenWidth, screenWidth);
                yPos = Random.Range(1, maxSpawnDistY);
                newPos = new Vector2(player.transform.position.x + xPos, player.transform.position.y - (screenHeight + yPos));
                break;
            case 2:
                xPos = Random.Range(1, maxSpawnDistX);
                yPos = Random.Range(-screenHeight, screenHeight);
                newPos = new Vector2(player.transform.position.x - (screenWidth + xPos), player.transform.position.y + yPos);
                break;
            case 3:
                xPos = Random.Range(1, maxSpawnDistX);
                yPos = Random.Range(-screenHeight, screenHeight);
                newPos = new Vector2(player.transform.position.x + screenWidth + xPos, player.transform.position.y + yPos);
                break;
            default:
                newPos = player.transform.position + (-player.transform.up * (screenHeight + 10));
                newPos.x = newPos.x + Random.Range(-3.0f, 3.0f);
                newPos.y = newPos.y + Random.Range(-3.0f, 3.0f);
                break;
        }

        //test the new spawn position against the camera view bounds - return false (do not spawn) if within teh camera bounds
        spawnPosTestObj.transform.position = newPos;

        if (GeometryUtility.TestPlanesAABB(GeometryUtility.CalculateFrustumPlanes(Camera.main), spawnPosTestObj.GetComponent<BoxCollider2D>().bounds))
        {
            spnPos = Vector2.zero;
            return false;
        }

        //return the new spawn position
        spnPos = spawnPosTestObj.transform.position;
        return true;
    }
}
