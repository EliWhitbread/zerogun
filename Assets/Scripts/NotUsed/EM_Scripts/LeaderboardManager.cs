﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeaderboardManager : MonoBehaviour
{
    private List<GameObject> userEntryPool = new List<GameObject>();
    private Dictionary<string, int> userScores;
    public GameObject userElements;

    public GameObject scrollBar;

    [HideInInspector]
    public GameObject userPrefab;

    /// <summary>
    /// This is the loading method for leaderboard - use when the leaderboard should be shown.
    /// </summary>
    public void CreateLeaderboard()
    {
        Initialisation();

        // If not enough entries exist in pool, created required amount.
        if (userEntryPool.Count < userScores.Count)
        {
            int poolCount = userEntryPool.Count, userCount = userScores.Count;
            for (int i = 0; i < (userCount - poolCount) + 1; i++)
            {
                GameObject newEntry = Instantiate(userPrefab, userElements.transform);
                newEntry.gameObject.SetActive(false);
                newEntry.transform.GetChild(0).GetComponent<Text>().text = string.Empty;
                newEntry.transform.GetChild(2).GetComponent<Text>().text = string.Empty;
                userEntryPool.Add(newEntry);
            }
        }

        // This is where the leaderboard information will be loaded.
        var sortedDict = from entry in userScores orderby entry.Value descending select entry;

        foreach (KeyValuePair<string, int> user in sortedDict)
        {
            CreateLeaderboardEntry(user.Key, user.Value);
        }
    }

    void OnEnable()
    {
        Initialisation();
    }

    public void OnDisable()
    {
        RemoveLeaderboard();
    }

    /// <summary>
    /// Used to null the leaderboard, when it is closed (so to free the memory, and avoid duplicate entries later).
    /// </summary>
    public void RemoveLeaderboard()
    {
        userScores = null;
        transform.parent.GetComponent<MenuOptions>().currentUsers.Clear();
        foreach (Transform child in userElements.transform)
        {
            child.gameObject.SetActive(false);
            child.GetChild(0).GetComponent<Text>().text = string.Empty;
            child.GetChild(2).GetComponent<Text>().text = string.Empty;
        }

        ToggleScrollbarVisibility();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


    }

    /// <summary>
    /// Toggles scrollbar visibility. Is determined by whether the Leaderboard Panel is active (true:false)
    /// </summary>
    void ToggleScrollbarVisibility()
    {
        userElements.transform.parent.GetComponent<ScrollRect>().verticalNormalizedPosition = 1f;
        scrollBar.SetActive(gameObject.activeInHierarchy ? true : false);
    }

    /// <summary>
    /// Initialisation ensures that the user score dictionary is initialised.
    /// </summary>
    private void Initialisation()
    {
        if (null != userScores)
        {
            return;
        }

        if (null == userPrefab)
        {
            userPrefab = transform.parent.GetComponent<MenuOptions>().leaderboardUserPrefab;
        }

        // Initialise User Entry Pool
        if (userElements.transform.childCount == 0)
        {
            for (int i = 0; i < transform.parent.GetComponent<MenuOptions>().initialUserPoolSize; i++)
            {
                GameObject newEntry = Instantiate(userPrefab, userElements.transform);
                newEntry.SetActive(false);
                newEntry.transform.GetChild(0).GetComponent<Text>().text = string.Empty;
                newEntry.transform.GetChild(2).GetComponent<Text>().text = string.Empty;
                userEntryPool.Add(newEntry);
            }
        }


        ToggleScrollbarVisibility();

        userScores = new Dictionary<string, int>();
    }

    /// <summary>
    /// Adds a user to the leaderboard. Int score isn't necessary, unless it is needed as the user is created.
    /// </summary>
    /// <param name="username">Set the new user's username</param>
    /// <param name="score">Optionally also add score</param>
    /// <returns></returns>
    public bool AddUser(string username, int score = 0)
    {
        Initialisation();

        if (userScores.ContainsKey(username) == true)
        {
            // Username already exists
            return false;
        }

        userScores.Add(username, score);

        if (transform.parent.GetComponent<MenuOptions>().currentUsers != null)
        {
            transform.parent.GetComponent<MenuOptions>().currentUsers.Add(username);
        }

        return true;
    }

    /// <summary>
    /// Generically creates user entries, from the given prefab.
    /// </summary>
    /// <param name="username"></param>
    /// <param name="score"></param>
    private void CreateLeaderboardEntry(string username, int score)
    {
        for (int i = 0; i < userEntryPool.Count; i++)
        {
            if (!userEntryPool[i].activeInHierarchy)
            {
                userEntryPool[i].transform.GetChild(0).GetComponent<Text>().text = username;
                userEntryPool[i].transform.GetChild(2).GetComponent<Text>().text = score.ToString();
                userEntryPool[i].SetActive(true);
                break;
            }
        }
    }

    /// <summary>
    /// Gets the input users current score.
    /// </summary>
    /// <param name="username"></param>
    /// <returns></returns>
    public int GetScore(string username)
    {
        Initialisation();

        if (userScores.ContainsKey(username) == false)
        {
            return 0;
        }

        return userScores[username];
    }

    /// <summary>
    /// Sets a users score, though this is used internally by the ChangeScore method.
    /// </summary>
    /// <param name="username"></param>
    /// <param name="value"></param>
    private void SetScore(string username, int value)
    {
        Initialisation();

        if (userScores.ContainsKey(username) == false)
        {
            return;
        }

        userScores[username] = value;
    }

    /// <summary>
    /// Method for changing a users score. New score will be added to current score.
    /// </summary>
    /// <param name="username">Username</param>
    /// <param name="amount">New score</param>
    public void ChangeScore(string username, int amount)
    {
        Initialisation();
        int currentScore = GetScore(username);
        SetScore(username, currentScore + amount);
    }
}
