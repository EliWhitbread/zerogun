﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.UI;

//public class PauseManager : MonoBehaviour 
//{
//	[SerializeField]
//	GameObject pausePanel;
//	[SerializeField]
//	GameObject pauseButton;
//	[SerializeField]
//	GameObject bigPauseButton;
//	[SerializeField]
//	GameObject countDownObj;
//	[SerializeField]
//	Text countDownText;

//	float countDownBase = 3f;
//	[SerializeField]
//	float countDown = 0f;
//	[SerializeField]
//	bool beginCountdown = false;

//	public void Awake()
//	{
//		countDown = countDownBase;
//	}

//	public void OnApplicationPause (bool paused)
//	{
//		if (pausePanel)
//		{
//			if (paused)
//			{
//				PauseGame();
//			}
//			else
//			{
//				ContinueGame();
//			}
//		}
//	}

//	public void SetTimeScale(float timeScalePeram)
//	{
//		Time.timeScale = timeScalePeram;
//	}
	
//	public void Update()
//	{
//		if (beginCountdown)
//		{
//            StartCoroutine(ResumeCountdown());
//        }

//		if (countDown <= 0)
//		{
//			beginCountdown = false;
//			countDown = countDownBase;
//			ContinueGame();
//		}
//	}

//	public void Resume()
//	{
//		beginCountdown = true;
//	}

//	void PauseGame()
//	{
//		if (pausePanel && pauseButton)
//		{
//			pausePanel.SetActive(true);
//			pauseButton.SetActive(false);
//		}
//		Time.timeScale = 0;
//	}

//	void ContinueGame()
//	{
//		if (pausePanel && pauseButton)
//		{
//			pausePanel.SetActive(false);
//			pauseButton.SetActive(true);
//		}		
//		Time.timeScale = 1;
//	}

//    IEnumerator ResumeCountdown()
//    {
//        bigPauseButton.SetActive(false);
//        countDownObj.SetActive(true);
//        countDownText.text = "3";
//        yield return StartCoroutine(WaitForRealSeconds(0.5f));
//        countDownText.text = "2";
//        yield return StartCoroutine(WaitForRealSeconds(0.5f));
//        countDownText.text = "1";
//        yield return StartCoroutine(WaitForRealSeconds(0.5f));
//        countDownText.text = "";

//        ContinueGame();
//    }

//    IEnumerator WaitForRealSeconds(float waitTime)
//    {
//        float endTime = Time.realtimeSinceStartup + waitTime;

//        while (Time.realtimeSinceStartup < endTime)
//        {
//            yield return null;
//        }
//    }
//}
