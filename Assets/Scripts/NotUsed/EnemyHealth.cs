﻿//using UnityEngine;
//using System.Collections;

//public class EnemyHealth : MonoBehaviour {

//    public int hitPoints, score;
//    public GameObject player; //collisionChecker;
//    public float asteroidAutoDestroyDistance;

//    //Animator anim;
//    bool isActive; // hasAnimator = false;
//    protected float curHealth;

//    float customTick = 0.2f, curTime = 0.0f;

//   // CameraShake camShake;

//    [SerializeField]
//    float amountToShakeCam;

//    void Awake()
//    {
//        //anim = GetComponent<Animator>();
//        //if (anim != null)
//        //{
//        //    hasAnimator = true;
//        //}
//        //if (collisionChecker == null)
//        //{
//        //    collisionChecker = gameObject.transform.GetChild(0).gameObject;
//        //}
//        if(player == null)
//        {
//            player = GameObject.FindGameObjectWithTag("Player");
//        }

//        //if (camShake == null)
//        //{
//        //    camShake = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraShake>();
//        //}
//    }

//    void OnEnable()
//    {
        
//        curHealth = hitPoints;
//        if(isActive == false)
//        {
//            gameObject.GetComponent<CircleCollider2D>().enabled = true;
//            //collisionChecker.SetActive(false);
//        }
//        isActive = true;

//        //if (anim == null)
//        //{
//        //    hasAnimator = false;
//        //    anim = GetComponent<Animator>();
//        //    if (anim != null)
//        //    {
//        //        hasAnimator = true;
//        //    }
//        //}

//        ////float animSpeed = Random.Range(0.9f, 1.2f);
//        ////int dirChoice = Random.Range(0, 2);
//        ////if(dirChoice == 0)
//        ////{
//        ////    animSpeed *= -1;
//        ////}

//        //if(hasAnimator == true)
//        //{
//        //    anim.SetFloat("SpeedMultiplyer", animSpeed);
//        //}

//    }

//    void Update()
//    {
//        curTime += Time.deltaTime;
//        //disable Enemies if out of range of the Player
//        if(player != null && curTime >= customTick)
//        {
//            curTime = 0.0f;
//            Vector2 offset = player.transform.position - transform.position;
//            float sqrLength = offset.sqrMagnitude;
//            if(sqrLength > asteroidAutoDestroyDistance * asteroidAutoDestroyDistance)
//            {
//                gameObject.SetActive(false);
//            }
//        }
//    }
    
//    /// <summary>
//    /// apply damage to "enemy" (asteroid) object
//    /// </summary>
//    /// <param name="damage"></param>
//    public void TakeDamage(int damage)
//    {
//        curHealth -= damage;
//        if(curHealth <= 0 && isActive)
//        {
//            GameManager.curGameManager.UpdatePlayerScore(score);
//            DeactivateObject();
//            //EffectSpawner.curEffectSpawner.SpawnExplosion(transform.position);
//            //anim.SetBool("Explode", true);
//            ////GetComponent<SpriteRenderer>().enabled = false;
//            //EnemySpawner.curEnemySpawner.SpawnPickupAtAsteroidPos(transform.position);
//            //isActive = false;
//            //gameObject.GetComponent<CircleCollider2D>().enabled = false;
//            //collisionChecker.SetActive(false);
//        }
//    }

//    /// <summary>
//    /// called from Entity class on GameObject
//    /// </summary>
//    public void DeactivateObject()
//    {
//        EffectSpawner.curEffectSpawner.SpawnExplosion(transform.position);
//        //if (GameManager.curGameManager.PlayerAlive)
//        //{
//        //    camShake.ShakeDuration = amountToShakeCam;
//        //    camShake.ShakeCamera();
//        //}
//        //anim.SetBool("Explode", true); - new asteroids do not have animations... yet?
//        //GetComponent<SpriteRenderer>().enabled = false;
//        EnemySpawner.curEnemySpawner.SpawnPickupAtAsteroidPos(transform.position);
//        isActive = false;
//        gameObject.GetComponent<CircleCollider2D>().enabled = false;
//        //collisionChecker.SetActive(false);
//        DestroyEnemy();
//    }

//    void DestroyEnemy()
//    {
//        gameObject.SetActive(false);
//    }

    
//}
