﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OxygenWarningEffect : MonoBehaviour
{
    ParticleSystem pSystem;
    float oxygenLevel = 100.0f;

    public float OxygenLevel
    {
        set { oxygenLevel = value; }
    }

    public void Emit(bool emit)
    {
        if(emit)
        {
            pSystem.Play();
        }
        else
        {
            pSystem.Stop();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        pSystem = gameObject.GetComponent<ParticleSystem>();
    }

}
