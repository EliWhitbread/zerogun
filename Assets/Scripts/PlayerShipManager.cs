﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShipManager : MonoBehaviour
{
    [SerializeField] PlayerShipScriptableObject selectedPlayerShipScriptableObject;

    [SerializeField] PlayerShipScriptableObject[] playerShipScriptableObjects;


    void GetAvailablePlayerShips()
    {
         PlayerShipScriptableObject[] _tempShipArray = Resources.LoadAll<PlayerShipScriptableObject>("PlayerShips");
        //playerShipScriptableObjects = Resources.LoadAll<PlayerShipScriptableObject>("PlayerShips");

        playerShipScriptableObjects = new PlayerShipScriptableObject[_tempShipArray.Length];

        for (int i = 0; i < _tempShipArray.Length; i++)
        {
            for (int y = 0; y < _tempShipArray.Length; y++)
            {
                if (_tempShipArray[y].shipID == i)
                {
                    playerShipScriptableObjects[i] = _tempShipArray[y];

                    //add to ship selection menu
                    ShipDisplayInformation _shipInfo = new ShipDisplayInformation {
                        shipName = _tempShipArray[y].shipName,
                        shipDisplaySprite = _tempShipArray[y].mainShipSprite,
                        shipID = _tempShipArray[y].shipID
                    };

                    UIManager.curUIManager.AddShipToSelectionMenu(_shipInfo, i == _tempShipArray.Length - 1 ? true : false);

                }
            }
        }        
        
    }

    public PlayerShipScriptableObject GetShipData(bool randomSelection = false, int selection = 0)
    {
        if(playerShipScriptableObjects == null || playerShipScriptableObjects.Length < 1)
        {
            GetAvailablePlayerShips();
        }

        if(randomSelection == true)
        {
            int rnd = Random.Range(0, playerShipScriptableObjects.Length);
            return playerShipScriptableObjects[rnd];
        }

        return playerShipScriptableObjects[selection];
    }

}
