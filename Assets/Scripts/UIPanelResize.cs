﻿
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections;

public class UIPanelResize : MonoBehaviour
{
    RectTransform rect;
    TextMeshProUGUI uiText;

    [SerializeField] Vector2 elementPositionPortrait, elementSizePortrait, elementPositionLandscape, elementSizeLandscape;
    [SerializeField] bool changeTextElement;
    [SerializeField] float textSizePortrait, textSizeLandscape;

    // Start is called before the first frame update
    void OnEnable()
    {
        ScreenOrientationChecker.OnScreenOrientationChange += ScreenOrientationChecker_OnScreenOrientationChange;

        //set initial orientation
        StartCoroutine(SetInitialOrientation());
    }

    IEnumerator SetInitialOrientation()
    {
        yield return new WaitUntil(()=>ScreenOrientationChecker.orientationChecker != null);
        ScreenOrientationChecker_OnScreenOrientationChange(ScreenOrientationChecker.orientationChecker.CamLandscape);
    }

    private void OnDisable()
    {
        ScreenOrientationChecker.OnScreenOrientationChange -= ScreenOrientationChecker_OnScreenOrientationChange;
    }

    private void ScreenOrientationChecker_OnScreenOrientationChange(bool isLandscape)
    {
        if(rect == null)
        {
            rect = gameObject.GetComponent<RectTransform>();
        }
        if(uiText == null && changeTextElement == true)
        {
            uiText = gameObject.GetComponentInChildren<TextMeshProUGUI>();
        }

        if(changeTextElement == true)
        {
            uiText.fontSize = isLandscape ? textSizeLandscape : textSizePortrait;
        }        

        rect.sizeDelta = isLandscape ? elementSizeLandscape : elementSizePortrait;
        rect.anchoredPosition = isLandscape ? elementPositionLandscape : elementPositionPortrait;
        rect.ForceUpdateRectTransforms();

    }

}
