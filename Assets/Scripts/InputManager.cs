﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

    protected static InputManager _current { get; private set; }

    public static InputManager Current
    {
        get { return _current; }
    }

    public delegate void OnTapDetected(bool isTouch);
    public static event OnTapDetected OnTap;
    public delegate void OnTapEnded();
    public static event OnTapEnded OnTapEnd;


    bool touchDetected = false;

    private void Awake()
    {
        if(_current != null && _current != this)
        {
            Destroy(gameObject);
            return;
        }
        _current = this;
    }

    
	void Update ()
    {
#if UNITY_IOS || UNITY_ANDROID

        if (Input.touchCount > 0 && touchDetected == false)
        {
            if (OnTap != null)
            {
                OnTap(true);
            }
            touchDetected = true;
        }
        if (Input.touchCount < 1 && touchDetected == true)
        {
            if (OnTapEnd != null)
            {
                OnTapEnd();
            }
            touchDetected = false;
        }

#endif
#if UNITY_WEBGL
    
        if (Input.GetButtonDown("Fire1"))
        {
            if (OnTap != null)
            {
                OnTap(false);
            }
        }
        if (Input.GetButtonUp("Fire1"))
        {
            if (OnTapEnd != null)
            {
                OnTapEnd();
            }
        }

#endif
#if UNITY_EDITOR
        if (Input.GetButtonDown("Fire1"))
        {
            if (OnTap != null)
            {
                OnTap(false);
            }
        }
        if (Input.GetButtonUp("Fire1"))
        {
            if (OnTapEnd != null)
            {
                OnTapEnd();
            }
        }
#endif
#if UNITY_STANDALONE_OSX
        if (Input.GetButtonDown("Fire1"))
        {
            if (OnTap != null)
            {
                OnTap(false);
            }
        }
        if (Input.GetButtonUp("Fire1"))
        {
            if (OnTapEnd != null)
            {
                OnTapEnd();
            }
        }
#endif
#if UNITY_STANDALONE_WIN
        if (Input.GetButtonDown("Fire1"))
        {
            if (OnTap != null)
            {
                OnTap(false);
            }
        }
        if (Input.GetButtonUp("Fire1"))
        {
            if (OnTapEnd != null)
            {
                OnTapEnd();
            }
        }
#endif
    }
}
