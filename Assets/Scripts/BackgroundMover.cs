﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Background Parallax scrolling
/// </summary>
public class BackgroundMover : MonoBehaviour
{

    public float parralax;

    MeshRenderer meshRndr;
    Material mat;
    Vector2 matOffset;


    void Start()
    {
        meshRndr = GetComponent<MeshRenderer>();
        mat = meshRndr.material;
        matOffset = mat.mainTextureOffset;

    }

    void Update()
    {
        matOffset.x = transform.position.x / transform.localScale.x / parralax;
        matOffset.y = transform.position.y / transform.localScale.y / parralax;

        mat.mainTextureOffset = matOffset;

    }
}
