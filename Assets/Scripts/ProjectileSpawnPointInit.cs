﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileSpawnPointInit : MonoBehaviour {

    [SerializeField]GameObject laserOrigin_Base, laserOrigin_Large;

    private void OnEnable()
    {
        WeaponManager.curWeaponManager.ProjectileSpawnInit = this;

        if(laserOrigin_Base == null)
        {
            laserOrigin_Base = gameObject.transform.Find("Laser_Origin_Base").gameObject;
        }
        if(laserOrigin_Large == null)
        {
            laserOrigin_Large = gameObject.transform.Find("Laser_Origin_Large").gameObject;
        }

    }

    public void ToggleLaserOriginEffect(bool showEfect = false, bool isBaseLaser = true)
    {
        if(!showEfect)
        {
            laserOrigin_Base.SetActive(false);
            laserOrigin_Large.SetActive(false);
            return;
        }

        laserOrigin_Base.SetActive(isBaseLaser);
        laserOrigin_Large.SetActive(!isBaseLaser);
    }
}
