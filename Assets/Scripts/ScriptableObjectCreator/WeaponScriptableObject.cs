﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="NewWeapon", menuName ="NewWeapon")]
public class WeaponScriptableObject : ScriptableObject {

    public string weaponName = "New Weapon";
    public Weapon weapon = Weapon.Base;
    public float fireDelay = 0.2f;
    public float shotMovementForce;
    public float[] projectileSpawnRotations = new float[] {0.0f};
    public GameObject projectile;
    public int ammunition;

}
