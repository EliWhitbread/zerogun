﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewPlayerShip", menuName = "NewShip")]
public class PlayerShipScriptableObject : ScriptableObject
{
    public string shipName = "NewShip";
    [Header("Ship Variables")]
    public float moveSpeed = 0.5f;
    public float slowDownMoveMultiplyer = 0.8f;
    public float floatSpeed = 1.0f;
    public float floatSpeedIncrement = 0.002f;
    public float shieldActiveTime = 1.5f;
    public float shipRotationSpeed = 15.0f;
    public float shipInertiaAmount = 0.0f;
    public float shipOxygenDecrimentRate = 1.50f;
    public int crashDamage = 100;

    [Header("Main Ship")]
    public Sprite mainShipSprite;
    public Vector2 projectileFirePointOffset = Vector3.zero;
    public Vector2[] mainShipPolygonCollider;
    

    [Header("Shield")]
    public int shieldSpriteAnimID;
    public Vector2 shieldColliderOffset = Vector3.zero;
    public float shieldColliderRadius = 0.85f;

    [Header("Menu Info")]
    public Sprite shipMenuDisplaySprite;
    public int shipID;

}
