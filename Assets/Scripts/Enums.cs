﻿
public enum EntityType { Asteroid, ShieldPickup, AmmoPickup }

public enum GameState { StartMenu, Play, Paused, ShipSelection }

public enum AudioEffectClip { AsteroidSmash, Laser, PlayerDeath, Projectile_Small, Projectile_Large }

public enum StackableWeapons { BIG_SHOT, LASER, SPREAD_SHOT }

public enum StackableWeaponPickupType { SIZE_TYPE, SHOT_TYPE, SPREAD_TYPE }

public enum ProjectileSize { BASE, LARGE }

public enum ProjectileType { PROJECTILE, LASER }

public enum ProjectileSpread { NONE, SPREAD }