﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileAnimEventListener : MonoBehaviour
{
    PlayerProjectileMover mover;

    private void OnEnable()
    {
        if(mover == null)
        {
            mover = gameObject.GetComponentInParent<PlayerProjectileMover>();
        }
    }

    public void AnimFinished()
    {
        mover.AnimFinished();
    }
}
