﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{

    public GameObject player;
    public float camMoveTime;

    void Start()
    {
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
    }

    void LateUpdate()
    {
        if (player != null)
        {
            float _newPosX = player.transform.position.x;
            float _newPosY = player.transform.position.y;

            Vector3 _newCamPos = new Vector3(_newPosX, _newPosY, transform.position.z);

            transform.position = Vector3.Lerp(transform.position, _newCamPos, camMoveTime * Time.deltaTime);
        }        
    }
}
