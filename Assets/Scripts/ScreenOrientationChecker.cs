﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Check the orientation of the device and alter the Camera orthographic size if required
/// </summary>
public class ScreenOrientationChecker : MonoBehaviour
{
    public static ScreenOrientationChecker orientationChecker { get; private set; }

    public delegate void ScreenOrientationChange(bool isLandscape);
    public static event ScreenOrientationChange OnScreenOrientationChange;

    Camera cam;
    float _camOrthoSize;
    bool _camLandscape;

    public bool CamLandscape
    {
        get { return _camLandscape; }
    }

    void Awake()
    {
        if(orientationChecker != null && orientationChecker != this)
        {
            Destroy(this);
            return;
        }

        orientationChecker = this;

        cam = Camera.main;

        _camOrthoSize = cam.orthographicSize;

        if (cam.aspect > 1)
        {
            cam.orthographicSize = _camOrthoSize;
            _camLandscape = true;
        }
        else
        {
            cam.orthographicSize = _camOrthoSize / cam.aspect;
            _camLandscape = false;
        }
    }

    private void Start()
    {
        UpdateScreenOrientation(_camLandscape);
    }

    void Update()
    {

        //if(Input.deviceOrientation == DeviceOrientation.Portrait)
        //{
        //    Camera.main.orthographicSize = _camOrthoSize / Camera.main.aspect;
        //}
        //else if(Input.deviceOrientation == DeviceOrientation.LandscapeLeft || Input.deviceOrientation == DeviceOrientation.LandscapeRight)
        //{
        //    Camera.main.orthographicSize = _camOrthoSize;
        //}

        if (cam.aspect > 1 && _camLandscape == false)
        {
            cam.orthographicSize = _camOrthoSize;
            _camLandscape = true;
            UpdateScreenOrientation(true);
        }
        if (cam.aspect <= 1 && _camLandscape == true)
        {
            cam.orthographicSize = _camOrthoSize / cam.aspect;
            _camLandscape = false;
            UpdateScreenOrientation(false);
        }

    }

    void UpdateScreenOrientation(bool screenIsLandscape)
    {
        if(OnScreenOrientationChange != null)
        {
            OnScreenOrientationChange(screenIsLandscape);
        }
    }
}
