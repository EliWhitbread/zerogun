﻿//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine.UI;

////public enum StackableWeapons
////{
////    BIG_SHOT, LASER, SPREAD_SHOT
////}

////public enum StackableWeaponPickupType
////{
////    SIZE_TYPE, SHOT_TYPE, SPREAD_TYPE
////}

////public enum ProjectileSize
////{
////    BASE, LARGE
////}

////public enum ProjectileType
////{
////    PROJECTILE, LASER
////}

////public enum ProjectileSpread
////{
////    NONE, SPREAD
////}

//public class PlayerGunManager : MonoBehaviour
//{

//    public static PlayerGunManager curPlayerGunManager { get; private set; }

//    public float shootDelay;
//    public Transform projectileSpawnPoint;
//    //public Text ammoText;

//    [Header("Projectile Pool")]
//    public GameObject projectile;
//    public GameObject largeProjectile;
//    public float poolSize;
//    List<GameObject> projectiles, largeProjectiles;
//    List<Vector3> projectileSpawnPositions;
//    Vector3 projectileSpawnOffset;

//    ProjectileSize currentProjectileSize = ProjectileSize.BASE;
//    ProjectileType currentProjectileType = ProjectileType.PROJECTILE;
//    ProjectileSpread currentProjectileSpread = ProjectileSpread.NONE;

//    public int playerAmmo, maxWeaponAmmo;

//    //laser
//    //LineRenderer laserLine;

//    //current weapon
//    //[SerializeField]
//    //WeaponStats currentWeaponStats;

//    [SerializeField]
//    WeaponData currentWeaponData;



//    float timer, speedAddAmount;
//    bool canShoot = false, playerControlEnabled = true;

//    Transform myTransform;

//    public WeaponData CurrentWeaponData
//    {
//        get { return currentWeaponData; }
//    }
//    //public WeaponStats WeaponStats
//    //{
//    //    get { return currentWeaponStats; }
//    //    set { currentWeaponStats = value;}
//    //}
//    /// <summary>
//    /// can the player shoot
//    /// </summary>
//    public bool CanShoot
//    {
//        get { return canShoot; }
//        set { canShoot = value; }

//    }

//    /// <summary>
//    /// enable/disable player gun control
//    /// </summary>
//    public bool PlayerControlEnabled
//    {
//        get { return playerControlEnabled; }
//        set { playerControlEnabled = value; }
//    }

//    /// <summary>
//    /// Available player amunition
//    /// </summary>
//    public int PlayerAmmo
//    {
//        get { return playerAmmo; }
//        set { playerAmmo = value; }
//    }

//    /// <summary>
//    /// set the projectile spawn position offset
//    /// </summary>
//    public Vector3 ProjectileSpawnOffset
//    {
//        set { projectileSpawnOffset = value; projectileSpawnOffset.z = 0.05f; projectileSpawnPoint.localPosition = projectileSpawnOffset; }
//    }

//    void Awake()
//    {
//        if (curPlayerGunManager != null && curPlayerGunManager != this)
//        {
//            Destroy(gameObject);
//            return;
//        }
//        curPlayerGunManager = this;
//        myTransform = this.transform;

//        //if (ammoText == null)
//        //{
//        //    ammoText = GameObject.Find("AmmoText").GetComponent<Text>();
//        //}

//        //laserLine = gameObject.GetComponent<LineRenderer>();
//    }

//    //void OnEnable()
//    //{
//    //    //ammoText.text = "Ammo: " + playerAmmo.ToString();
//    //    ammoText.enabled = false;
//    //    //currentWeaponStats = GameManager.curGameManager.availableWeapons[0]; // set to base weapon on enable

//    //}

//    void Start()
//    {

//        projectiles = new List<GameObject>();
//        largeProjectiles = new List<GameObject>();

//        for (int i = 0; i < poolSize; i++)
//        {
//            //base projectile pool
//            GameObject obj = (GameObject)Instantiate(projectile);
//            obj.SetActive(false);
//            projectiles.Add(obj);

//            //large projectile pool
//            GameObject obj1 = (GameObject)Instantiate(largeProjectile);
//            obj1.SetActive(false);
//            largeProjectiles.Add(obj1);
//        }

//        speedAddAmount = projectile.GetComponent<PlayerProjectileMover>().playerSpeedAddition;
//        //playerAmmo = GameManager.curGameManager.playerMaxAmmo;
//        //ammoText.text = "Ammo: " + playerAmmo.ToString();

//        //currentWeaponStats = GameManager.curGameManager.availableWeapons[0]; // set to base weapon on enable

//        currentWeaponData = WeaponDataHolder.curWeaponDataHolder.baseWeaponData;
//    }

//    void LateUpdate()
//    {
//        if (playerControlEnabled == false)
//        {
//            return;
//        }

//        if (canShoot)
//        {
//            //if (currentProjectileType == ProjectileType.LASER)
//            //{
//            //    Shoot();
//            //    return;
//            //}
//            timer += Time.deltaTime;
//            /*if (timer >= currentWeaponStats.fireDelay)*/ // && playerAmmo > 0)
//            if (timer >= currentWeaponData.fireDelay)
//            {
//                Shoot();
//                timer = 0;

//                //if (playerAmmo <= 0)
//                //{
//                //    PlayerController.curPlayerController.applyAcceleration = false;
//                //}
//            }

//        }
//        //else if(currentProjectileType == ProjectileType.LASER)
//        //{
//        //    laserLine.enabled = false;
//        //}

//    }

//    void Shoot()
//    {
//        if (projectileSpawnPoint == null)
//        {
//            projectileSpawnPoint = PlayerController.curPlayerController.GetProjectileSpawnPoint;
//        }

//        //if(currentProjectileType == ProjectileType.LASER)
//        //{
//        //    FireLaser(projectileSpawnPoint.transform.rotation);
//        //    return;
//        //}

//        if (currentProjectileSize == ProjectileSize.BASE)
//        {
//            FireProjectile(projectileSpawnPoint.transform.rotation, projectiles);
//        }
//        else
//        {
//            FireProjectile(projectileSpawnPoint.transform.rotation, largeProjectiles);
//        }

//    }


//    //void FireProjectile(Vector3 firePos)
//    void FireProjectile(Quaternion rot, List<GameObject> projectilePoolRef)
//    {
//        /*for (int r = 0; r < currentWeaponStats.shotAngles.Count; r++)*/ //allow for multiple shot angles
//        for (int r = 0; r < currentWeaponData.shootAngles.Count; r++)
//        {
//            for (int i = 0; i < projectilePoolRef.Count; i++)
//            {
//                if (!projectilePoolRef[i].activeInHierarchy)
//                {
//                    projectilePoolRef[i].transform.position = projectileSpawnPoint.position;
//                    //projectilePoolRef[i].transform.rotation = rot * Quaternion.Euler(0, 0, currentWeaponStats.shotAngles[r]);
//                    projectilePoolRef[i].transform.rotation = rot * Quaternion.Euler(0, 0, currentWeaponData.shootAngles[r]);
//                    projectilePoolRef[i].SetActive(true);
//                    //PlayerController.curPlayerController.UpdateMoveSpeed(currentWeaponStats.baseForceAppliedToPlayer);
//                    PlayerController.curPlayerController.UpdateMoveSpeed(currentWeaponData.playerMoveForce / currentWeaponData.shootAngles.Count);


//                    //no longer used - old limited ammo system
//                    //if (playerAmmo <= 0)
//                    //{
//                    //    GameManager.curGameManager.activateAmmoRecharge = true;
//                    //    GameManager.curGameManager.ammoRechargeEffect.SetActive(true);
//                    //}
//                    break;
//                }
//            }
//        }
//        AudioManager.audioManager.PlayAudio(currentProjectileSize == ProjectileSize.BASE ? AudioEffectClip.Projectile_Small : AudioEffectClip.Projectile_Large);
//        if (playerAmmo > 0)
//        {
//            playerAmmo--;

//            if (playerAmmo <= 0)
//            {
//                ResetWeapon();
//            }
//        }

//    }

//    void FireLaser(Quaternion rot)
//    {
//        for (int r = 0; r < currentWeaponData.shootAngles.Count; r++)
//        {
//            RaycastHit hit;
//            // laserLine.enabled = true;
//            // laserLine.SetPosition(0, projectileSpawnPoint.position);

//            Quaternion newAngle = rot * Quaternion.Euler(0, 0, currentWeaponData.shootAngles[r]);
//            Vector3 pos = projectileSpawnPoint.position;
//            Vector3 endPos = pos;
//            endPos.z = newAngle.z;

//            if (Physics.Raycast(projectileSpawnPoint.position, projectileSpawnPoint.up + endPos, out hit, 10.0f))
//            {
//                // laserLine.SetPosition(1, hit.point);
//            }
//            else
//            {
//                // laserLine.SetPosition(1, projectileSpawnPoint.up + endPos);
//            }

//            //StartCoroutine(DisableLaser());
//        }

//    }

//    IEnumerator DisableLaser()
//    {
//        yield return new WaitForSecondsRealtime(currentWeaponData.lifeTime);

//        // laserLine.enabled = false;
//    }


//    public void AddAmmo(int ammo)
//    {
//        playerAmmo += ammo;
//        //ammoText.text = "Ammo: " + playerAmmo.ToString();
//    }

//    public void UpdateProjectileSpawnPoints(List<Vector2> newSpawnPositions)
//    {
//        //projectileSpawnPositions = new List<Vector3>();
//        if (projectileSpawnPositions == null)
//        {
//            projectileSpawnPositions = new List<Vector3>();
//        }
//        else
//        {
//            projectileSpawnPositions.Clear();
//        }

//        newSpawnPositions.ForEach((p) => { projectileSpawnPositions.Add(p); });

//    }

//    public void ResetWeapon()
//    {
//        currentProjectileSize = ProjectileSize.BASE;
//        currentProjectileType = ProjectileType.PROJECTILE;
//        currentProjectileSpread = ProjectileSpread.NONE;
//        currentWeaponData = WeaponDataHolder.curWeaponDataHolder.baseWeaponData;
//    }

//    public void SetupWeapon(StackableWeaponPickupType puType, StackableWeapons puWeapon)
//    {
//        switch (puType)
//        {
//            case StackableWeaponPickupType.SIZE_TYPE:
//                currentProjectileSize = puWeapon == StackableWeapons.BIG_SHOT ? ProjectileSize.LARGE : ProjectileSize.BASE;
//                if (puWeapon == StackableWeapons.BIG_SHOT)
//                {
//                    currentWeaponData = WeaponDataHolder.curWeaponDataHolder.bigShotWeaponData;
//                    if (currentProjectileSpread == ProjectileSpread.SPREAD)
//                    {
//                        currentWeaponData.shootAngles = WeaponDataHolder.curWeaponDataHolder.spreadShotWeaponData.shootAngles;
//                    }
//                    playerAmmo = currentWeaponData.maxAmmo;
//                }
//                break;
//            case StackableWeaponPickupType.SHOT_TYPE:
//                currentProjectileType = puWeapon == StackableWeapons.LASER ? ProjectileType.LASER : ProjectileType.PROJECTILE;
//                break;
//            case StackableWeaponPickupType.SPREAD_TYPE:
//                currentProjectileSpread = puWeapon == StackableWeapons.SPREAD_SHOT ? ProjectileSpread.SPREAD : ProjectileSpread.NONE;
//                if (puWeapon == StackableWeapons.SPREAD_SHOT)
//                {
//                    currentWeaponData.shootAngles = WeaponDataHolder.curWeaponDataHolder.spreadShotWeaponData.shootAngles;
//                    playerAmmo = WeaponDataHolder.curWeaponDataHolder.spreadShotWeaponData.maxAmmo;
//                }
//                break;
//            default:
//                break;
//        }
//    }


//}
