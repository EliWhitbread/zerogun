﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour
{

    public static EnemySpawner curEnemySpawner { get; private set; }

    [SerializeField]
    GameObject[] asteroid;
    public GameObject player, spawnPosTestObj;
    //pickup
    public GameObject Pickup_Prefab;

    public int asteroidsToPool, initialAsteroidPlacementCount, shieldPickUpsToPool, ammoPickUpsToPool, healthPickUpsToPool, pickupsToPool;
    public float asteroidSpawnInterval, curAsteroidSpawnInterval, initialDistToPlayerBuffer, maxSpawnDistX, maxSpawnDistY, screenHeight, screenWidth, maxPickupSpawnDelay, curPickupSpawnDelay, curEnemyShipSpawnDelay;

    List<GameObject> asteroidPool;
    List<GameObject> pickupPool;
    
    int asteroidPickupSpawnRandomMax = 5;
    

    Camera sceneCamera;

    float myTime, myPickupSpawnTime;
    bool canSpawn, spawnPickups;

    public bool CanSpawn
    {
        get { return canSpawn; }
        set { canSpawn = value; }
    }

    public bool SpawnPickups
    {
        set { spawnPickups = value; }
    }

    void Awake()
    {
        if(curEnemySpawner != null && curEnemySpawner != this)
        {
            Destroy(gameObject);
            return;
        }
        curEnemySpawner = this;
        canSpawn = true;
    }

    void Start()
    {
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }

        FillPools();

        CalculateCameraBounds();
        curAsteroidSpawnInterval = 0.3f;
        curPickupSpawnDelay = maxPickupSpawnDelay;
        asteroidPickupSpawnRandomMax = 5;


    }

    void CalculateCameraBounds()
    {
        sceneCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

        screenHeight = sceneCamera.orthographicSize;
        screenWidth = screenHeight * sceneCamera.aspect;
    }

    void FillPools()
    {
        asteroidPool = new List<GameObject>();
        pickupPool = new List<GameObject>();

        for (int i = 0; i < asteroidsToPool; i++)
        {
            int asteroidNum = Random.Range(0, 5);

            switch (asteroidNum)
            {
                case 0:
                    GameObject obj = Instantiate(asteroid[0]) as GameObject;
                    obj.SetActive(false);
                    asteroidPool.Add(obj);
                    break;
                case 1:
                    GameObject obj1 = Instantiate(asteroid[1]) as GameObject;
                    obj1.SetActive(false);
                    asteroidPool.Add(obj1);
                    break;
                case 2:
                    GameObject obj2 = Instantiate(asteroid[2]) as GameObject;
                    obj2.SetActive(false);
                    asteroidPool.Add(obj2);
                    break;
                case 3:
                    GameObject obj3 = Instantiate(asteroid[3]) as GameObject;
                    obj3.SetActive(false);
                    asteroidPool.Add(obj3);
                    break;
                case 4:
                    GameObject obj4 = Instantiate(asteroid[4]) as GameObject;
                    obj4.SetActive(false);
                    asteroidPool.Add(obj4);
                    break;
            }
            
        }

        InitialAsteroidSpawn();

        for (int p = 0; p < pickupsToPool; p++)
        {
            GameObject obj = Instantiate(Pickup_Prefab) as GameObject;
            obj.SetActive(false);
            pickupPool.Add(obj);
        }
    }

    void InitialAsteroidSpawn()
    {
        Vector2 pos = Vector2.zero;

        for (int i = 0; i < initialAsteroidPlacementCount; i++)
        {
            if (GetInitialPlacementPosition(out pos))
            {
                if (!asteroidPool[i].activeInHierarchy)
                {
                    asteroidPool[i].transform.position = pos;
                    asteroidPool[i].transform.rotation = Quaternion.identity;
                    asteroidPool[i].name = "Asteroid";
                    asteroidPool[i].SetActive(true);
                }
            }
            else
            {
                i--;
            }
        }

    }

    bool GetInitialPlacementPosition(out Vector2 spawnPos)
    {
        float xPos = Random.Range(player.transform.position.x - 5.0f, player.transform.position.x + 5.0f);
        float yPos = Random.Range(player.transform.position.y - 5.0f, player.transform.position.y + 5.0f);

        Vector3 pos = new Vector3(xPos, yPos, 0);

        Vector2 offset = player.transform.position - pos;
        float sqrLength = offset.sqrMagnitude;

        if (sqrLength < initialDistToPlayerBuffer * initialDistToPlayerBuffer)
        {
            spawnPos = Vector2.zero;
            return false;
        }

        spawnPos = pos;
        return true;
    }

    void Update()
    {
        if(canSpawn == false)
        {
            return;
        }

        float t = Time.deltaTime;
        myTime += t;
        myPickupSpawnTime += t;

        if (myTime >= curAsteroidSpawnInterval)
        {
            SpawnAsteroid();
        }

        //don't spawn puckups while menu is active - set in GameManager
        if(spawnPickups == false)
        {
            return;
        }

        if (myPickupSpawnTime >= curPickupSpawnDelay)
        {
            SelectRandomPickUp();
        }

    }

    public void SpawnTimeUpdate(float value)
    {
        curAsteroidSpawnInterval = Mathf.Clamp((asteroidSpawnInterval / (value * 10)), 0.05f, 10.0f);
        curPickupSpawnDelay = Mathf.Clamp((maxPickupSpawnDelay / value), 0.5f, 20.0f);
    }

    void SpawnAsteroid()
    {
        Vector2 spawnPos = Vector2.zero;
        if (FindSpawnPosition(out spawnPos))
        {
            myTime = 0;

            for (int i = 0; i < asteroidPool.Count; i++)
            {
                if (!asteroidPool[i].activeInHierarchy)
                {
                    asteroidPool[i].transform.position = spawnPos;
                    asteroidPool[i].transform.rotation = Quaternion.identity;
                    asteroidPool[i].name = "Asteroid";
                    asteroidPool[i].SetActive(true);
                    return;
                }

            }
        }
    }

    /// <summary>
    /// spawn a pickup at a random position
    /// </summary>
    void SelectRandomPickUp()
    {
        Vector2 puSpawnPos = Vector2.zero;
        if (FindSpawnPosition(out puSpawnPos))
        {
            myPickupSpawnTime = 0;

            int rnd = Random.Range(0, 2);

            switch (rnd)
            {
                case 0:
                    SpawnPickup(puSpawnPos);
                    break;
                case 1:
                    SpawnPickup(puSpawnPos, true);
                    break;
            }
        }
    }

    /// <summary>
    /// Spawn a pickup at a defined position
    /// </summary>
    /// <param name="spnPos"></param>
    public void SpawnPickupAtAsteroidPos(Vector2 spnPos)
    {
        if(spawnPickups == false)
        {
            return;
        }

        int rand = Random.Range(0, asteroidPickupSpawnRandomMax);
        switch (rand)
        {
            case 0:
                SpawnPickup(spnPos, false, true); //not weapon
                asteroidPickupSpawnRandomMax = 6;
                break;
            case 1:
                SpawnPickup(spnPos, true); //is weapon
                asteroidPickupSpawnRandomMax = 6;
                break;
            default:
                asteroidPickupSpawnRandomMax--;
                asteroidPickupSpawnRandomMax = Mathf.Clamp(asteroidPickupSpawnRandomMax, 2, 6);
                break;
        }

    }

    void SpawnPickup(Vector2 pos, bool isGunPickup = false, bool oxygenAllowed = false)
    {
        myPickupSpawnTime = 0;

        for (int i = 0; i < pickupPool.Count; i++)
        {

            if (!pickupPool[i].activeInHierarchy)
            {
                Pickup puScript = pickupPool[i].GetComponent<Pickup>();

                bool canSpawn = true;

                if (isGunPickup == true)
                {
                    int r = Random.Range(0, 14);

                    puScript.puType = Pickup.PickupType.Weapon;
                    //number of available gun pickups

                    switch (r)
                    {
                        case int num when num < 4:
                            puScript.thisWeaponType = StackableWeapons.BIG_SHOT;
                            break;
                        case int num when num >= 4 && num < 8:
                            puScript.thisWeaponType = StackableWeapons.LASER;
                            break;
                        case int num when num >= 8 && num <= 10:
                            puScript.thisWeaponType = StackableWeapons.SPREAD_SHOT;
                            break;
                        default:
                            canSpawn = false;
                            break;
                    }
                }
                else
                {
                    int r = Random.Range(0, 20);

                    switch (r)
                    {
                        case int num when num < 4:
                            puScript.puType = Pickup.PickupType.Shield;                            
                            break;
                        case int num when num >= 4 && num < 6:
                            puScript.puType = Pickup.PickupType.SlowTime;
                            break;
                        case int num when num >= 6 && num < 15:
                            if (oxygenAllowed)
                            {
                                puScript.puType = Pickup.PickupType.Crystal;
                            }
                            break;
                        default:
                            canSpawn = false;
                            break;
                    }
                }
                if(canSpawn == false)
                {
                    return;
                }

                pickupPool[i].transform.position = pos;
                pickupPool[i].transform.rotation = Quaternion.identity;
                pickupPool[i].SetActive(true);

                return;
            }

        }
    }

    /// <summary>
    /// Find a suitable spawn position slightly favoring the direction of player movement
    /// </summary>
    /// <param name="spnPos"></param>
    /// <returns></returns>
    bool FindSpawnPosition(out Vector2 spnPos)
    {
        float xPos = 0.0f, yPos = 0.0f;
        Vector2 newPos = Vector2.zero;

        // 0 = up, 1 = down, 2 = left, 3 = right, default = up
        int randomDirChoice = Random.Range(0, 6);

        if (player != null)
        {
            switch (randomDirChoice)
            {
                case 0:
                    xPos = Random.Range(-screenWidth, screenWidth);
                    yPos = Random.Range(1, maxSpawnDistY);
                    newPos = new Vector2(player.transform.position.x + xPos, player.transform.position.y + screenHeight + yPos);
                    break;
                case 1:
                    xPos = Random.Range(-screenWidth, screenWidth);
                    yPos = Random.Range(1, maxSpawnDistY);
                    newPos = new Vector2(player.transform.position.x + xPos, player.transform.position.y - (screenHeight + yPos));
                    break;
                case 2:
                    xPos = Random.Range(1, maxSpawnDistX);
                    yPos = Random.Range(-screenHeight, screenHeight);
                    newPos = new Vector2(player.transform.position.x - (screenWidth + xPos), player.transform.position.y + yPos);
                    break;
                case 3:
                    xPos = Random.Range(1, maxSpawnDistX);
                    yPos = Random.Range(-screenHeight, screenHeight);
                    newPos = new Vector2(player.transform.position.x + screenWidth + xPos, player.transform.position.y + yPos);
                    break;
                default:
                    newPos = player.transform.position + (-player.transform.up * (screenHeight + 10));
                    newPos.x = newPos.x + Random.Range(-3.0f, 3.0f);
                    newPos.y = newPos.y + Random.Range(-3.0f, 3.0f);
                    break;
            }
        }        
        
        //test the new spawn position against the camera view bounds - return false (do not spawn) if within teh camera bounds
        spawnPosTestObj.transform.position = newPos;

        if (GeometryUtility.TestPlanesAABB(GeometryUtility.CalculateFrustumPlanes(sceneCamera), spawnPosTestObj.GetComponent<BoxCollider2D>().bounds))
        {
            spnPos = Vector2.zero;
            return false;
        }

        //return the new spawn position
        spnPos = spawnPosTestObj.transform.position;
        return true;
    }

    /// <summary>
    /// Disable active spawned GameObjects.
    /// </summary>
    /// <param name="clearAll">disable all or ONLY objects outside of the camera's display bounds.</param>
    public void ClearSpawnedObjects(bool clearAll)
    {
        DisableObjectsInList(asteroidPool, clearAll);
        DisableObjectsInList(pickupPool, true);
    }

    void DisableObjectsInList (List<GameObject> listRef, bool clearAll)
    {
        foreach (GameObject obj in listRef)
        {
            if(obj.activeInHierarchy)
            {
                if(clearAll == true)
                {
                    obj.SetActive(false);
                }
                else
                {
                    //disable only active GameObjects that are not within the camera's display bounds
                    if(!GeometryUtility.TestPlanesAABB(GeometryUtility.CalculateFrustumPlanes(sceneCamera), obj.GetComponent<Collider2D>().bounds))
                    {
                        obj.SetActive(false);
                    }
                }
                
            }
            
        }
    }

   
}
