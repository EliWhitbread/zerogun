﻿using UnityEngine;
using System.Collections;

public class EnemyMover : MonoBehaviour
{

    // public float moveSpeedX, moveSpeedY;

    //vector 2 to hold both x and y movement
    public Vector2 _acceleration;
    public float minRotationSpeed, MaxRotationSpeed;

    //property for gigid body 2d
    public Rigidbody2D RigBod { get { return rigBod; } set { rigBod = value; } }
    private Rigidbody2D rigBod;
    float rotSpeed;

    public float RotationSpeed
    {
        set { rotSpeed = value; }
    }

    void Awake()
    {
        if(rigBod == null)
        {
            rigBod = GetComponent<Rigidbody2D>();
        }
        
    }

    void OnEnable()
    {
        ////set a random move direction
        //moveSpeedX = Random.Range(-0.1f, 0.11f);
        //moveSpeedY = Random.Range(-0.1f, 0.11f);

        _acceleration.x = Random.Range(-0.5f, 0.51f);
        _acceleration.y = Random.Range(-0.5f, 0.51f);
        //Movement();

        rotSpeed = Random.Range(minRotationSpeed, MaxRotationSpeed);
        int rnd = Random.Range(0, 2);
        rotSpeed *= rnd == 0 ? -1 : 1;
        //StartCoroutine(EnableAsteroid());
    }

    IEnumerator EnableAsteroid()
    {
        _acceleration.x = Random.Range(-0.5f, 0.51f);
        _acceleration.y = Random.Range(-0.5f, 0.51f);
        rotSpeed = Random.Range(minRotationSpeed, MaxRotationSpeed);
        int rnd = Random.Range(0, 2);
        rotSpeed *= rnd == 0 ? -1 : 1;

        yield return new WaitForFixedUpdate();
        // Movement();
    }

    
    void Update()
    {
        transform.Translate(_acceleration * Time.deltaTime, Space.World);
        transform.RotateAround(transform.position, Vector3.forward, rotSpeed * Time.deltaTime);
    }

    private void Movement()
    {
        //apply small vector on enable to get asteroid moving
        rigBod.velocity += _acceleration;
    }

    public void AddVelocity(Vector2 dir)
    {
        //apply a force in vector 2 direction when called upon from other class
        rigBod.AddForce(dir);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Enemy"))
        {
            _acceleration.x *= -1;
            _acceleration.y *= -1;
        }
    }

}
