﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public static CameraShake curCamShake;

    // Transform of the camera to shake. Grabs the gameObject's transform
    // if null.
    Transform camTransform;
    IEnumerator shakeCoroutine;

    // How long the object should shake for.
    [SerializeField]
    float shakeDuration = 0f;

    // Amplitude of the shake. A larger value shakes the camera harder.
    [SerializeField]
    float shakeAmount = 0.7f;
    [SerializeField]
    float decreaseFactor = 1.0f;

    public float ShakeDuration
    {
        get
        {
            return shakeDuration;
        }

        set
        {
            shakeDuration = value;
        }
    }

    public float ShakeAmount
    {
        get
        {
            return shakeAmount;
        }

        set
        {
            shakeAmount = value;
        }
    }

    void Awake()
    {
        if (camTransform == null)
        {
            camTransform = GetComponent(typeof(Transform)) as Transform;
        }

        shakeCoroutine = Shake();
    }

    public void ShakeCamera(bool shakeCam = true)
    {
        if(shakeCam == false)
        {
            StopCoroutine("Shake");
            ShakeDuration = 0.0f;
            return;
        }
        else
        {
            StartCoroutine("Shake");
        }
    }

    IEnumerator Shake()
    {
        while (shakeDuration > 0.0f)
        {
            camTransform.localPosition = camTransform.localPosition + (Vector3)Random.insideUnitCircle * shakeAmount;
            shakeDuration -= Time.deltaTime * decreaseFactor;
            yield return null;
        }

        shakeDuration = 0f; // just incasse
    }

    //void Update()
    //{
    //    if (shakeDuration > 0)
    //    {
    //        camTransform.localPosition = (Vector2)camTransform.localPosition + Random.insideUnitCircle * shakeAmount;

    //        shakeDuration -= Time.deltaTime * decreaseFactor;
    //    }
    //    else
    //    {
    //        shakeDuration = 0f;
    //    }
    //}
}
