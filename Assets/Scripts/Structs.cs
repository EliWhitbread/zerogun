﻿
[System.Serializable]
public struct EntityStats
{
    public EntityType myEntityType;

    public int hitPoints, scoreValue, ValueIncrement;
}