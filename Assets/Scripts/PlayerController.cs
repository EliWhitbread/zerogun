﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour
{

    public static PlayerController curPlayerController { get; private set; }

    public GameObject playerSprite;

    int currentShipID;

    //public GameObject shieldParticleEffect;
    //[SerializeField]
    //GameObject shieldOutline;
    [SerializeField]
    GameObject shieldBounds;
    public float moveSpeed, slowDownMoveMultiplyer, floatSpeed, floatSpeedIncrementor, shipRotationSpeed, shipInertiaAmount, oxygenUseRate; // shieldCooldownTime
    public int crashDamage;
    public bool applyAcceleration, touchInputDetected;

    bool playerCanMove, shieldActive, fingerDown; //, shieldCooldown;
    float myTime, startMoveSpeed, oxygenLevel; //, currentShieldCooldownTime; // shieldAlpha;
    //ParticleSystem psMain; //- player no longer uses particles for Shield effect
    //Color psColour;
    Transform thisTransform, projectileSpawnPoint;
    bool checkForSpeedChange = true;

    //new weapon system test
    public WeaponScriptableObject weapon;

    //player shield
    SpriteRenderer shieldSpriteRnd;
    Animator shieldAnim;
    [SerializeField]int shieldAnimationID;

    CameraShake camShake;
    [SerializeField] float amountToShakeCam = 0.4f, camShakeDuration = 0.4f;
    //public bool ShieldCoolDown
    //{
    //    get { return shieldCooldown; }
    //    set { shieldCooldown = value; }
    //}

    public int ShieldAnimationId
    {
        set { shieldAnimationID = value; }
    }

    public bool PlayerCanMove
    {
        get { return playerCanMove; }
        set { playerCanMove = value; }
    }

    public int CurrentShipID
    {
        get { return currentShipID; }
        set { currentShipID = value; }
    }

    public Transform GetProjectileSpawnPoint
    {        
        get { if(projectileSpawnPoint == null)
            {
                projectileSpawnPoint = gameObject.GetComponentInChildren<ProjectileSpawnPointInit>().transform;
            }
            return projectileSpawnPoint;
        }
    }

    /// <summary>
    /// set player move speed - used to set an initial speed or override teh current (use UpdateMoveSpeed() for moveSpeed incriment)
    /// </summary>
    public float MoveSpeed
    {
        get { return moveSpeed; }
        set { moveSpeed = value; }
    }

    public float OxygenLevel
    {
        set { oxygenLevel += value; UIManager.curUIManager.UpdateOxygenBar(oxygenLevel / 100.0f); }
    }

    public void SetPlayerState(GameState curGameState)
    {
        moveSpeed = curGameState == GameState.StartMenu ? -1.0f : startMoveSpeed;
    }

    void Awake()
    {
        if (curPlayerController != null && curPlayerController != this)
        {
            Destroy(gameObject);
            return;
        }
        curPlayerController = this;
        startMoveSpeed = moveSpeed;
        fingerDown = false;

    }

    
    void OnEnable()
    {
        InputManager.OnTap += InputManager_OnTap;
        InputManager.OnTapEnd += InputManager_OnTapEnd;

        thisTransform = this.transform;
        thisTransform.rotation = Quaternion.identity;
        applyAcceleration = true;
        //shieldAlpha = 1.0f;
        checkForSpeedChange = true;

        shieldAnim = gameObject.GetComponentInChildren<Animator>();
        shieldSpriteRnd = shieldAnim.gameObject.GetComponent<SpriteRenderer>();
        camShake = Camera.main.GetComponent<CameraShake>();
        oxygenLevel = 100.0f;
        UIManager.curUIManager.UpdateOxygenBar(oxygenLevel / 100.0f);

        //test
        //WeaponManager.curWeaponManager.CurrentWeaponData = weapon;
    }

    public void ResetProjectileSpawnPointPos(float offset)
    {
        if(projectileSpawnPoint == null)
        {
            projectileSpawnPoint = gameObject.GetComponentInChildren<ProjectileSpawnPointInit>().transform;
        }
        Vector2 _pos = projectileSpawnPoint.localPosition;
        _pos.y = offset;
        projectileSpawnPoint.localPosition = _pos;
    }

    private void InputManager_OnTap(bool isTouch)
    {
        if (fingerDown == false)
        {
            //PlayerGunManager.curPlayerGunManager.CanShoot = false;

            //test
            WeaponManager.curWeaponManager.CanShoot = false;

            Vector3 tapPos = Vector3.zero;

            if (isTouch == false)
            {
                touchInputDetected = false;
                tapPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            }
            else
            {
                touchInputDetected = true;
                tapPos = Camera.main.ScreenToWorldPoint(Input.touches[0].position);
            }

            //Vector3 _wantRot = tapPos - transform.position;
            //float _rotz = Mathf.Atan2(_wantRot.y, _wantRot.x) * Mathf.Rad2Deg;
            //Quaternion targetRot = Quaternion.Euler(0, 0, _rotz);
            //transform.rotation = targetRot;

            if (!EventSystem.current.IsPointerOverGameObject())
            {
                fingerDown = true;
                StartCoroutine(SetCanFire());
            }
        }

    }

    IEnumerator SetCanFire()
    {
        yield return 0;
        //PlayerGunManager.curPlayerGunManager.CanShoot = true;
        //test
        WeaponManager.curWeaponManager.CanShoot = true;
    }

    private void InputManager_OnTapEnd()
    {
        StopCoroutine(SetCanFire());
        fingerDown = false;
        applyAcceleration = false;
        //PlayerGunManager.curPlayerGunManager.CanShoot = false;
        //test
        WeaponManager.curWeaponManager.CanShoot = false;

        checkForSpeedChange = true;
    }

    //Update the player move speed as well the Enemy spawn time and score multiplyer
    public void UpdateMoveSpeed(float value)
    {
        if (applyAcceleration == false)
        {
            applyAcceleration = true;
        }
        moveSpeed += value;
        floatSpeed += floatSpeedIncrementor;
        EnemySpawner.curEnemySpawner.SpawnTimeUpdate(moveSpeed);
        GameManager.curGameManager.SetScoreMultiplyer(moveSpeed);
    }

    //public void JoystickLook(Vector3 val)
    //{
    //    //test
    //    thisTransform.eulerAngles = new Vector3(0.0f, 0.0f, Mathf.Atan2(val.x, val.z) * -180 / Mathf.PI);

    //    //check for playerCanMove / canShoot / ??

    //    //float _rotz = Mathf.Atan2(val.y, val.x) * Mathf.Rad2Deg;
    //    //Quaternion targetRot = Quaternion.Euler(0, 0, _rotz - 90);
    //    //if (checkForSpeedChange == true && moveSpeed > 1.5f)
    //    //{
    //    //    checkForSpeedChange = false;
    //    //    float diff = Quaternion.Angle(thisTransform.rotation, targetRot);
    //    //    float speedEffect = (moveSpeed / 2.0f) / 180.0f;
    //    //    speedEffect *= diff;
    //    //    moveSpeed -= speedEffect;
    //    //}
    //    //thisTransform.rotation = Quaternion.Slerp(thisTransform.rotation, targetRot, Time.deltaTime * shipRotationSpeed);
    //}

    void Update()
    {

        if (playerCanMove == false)
        {
            thisTransform.Translate(0, -moveSpeed * Time.deltaTime, 0, Space.Self);
            return;
        }

        if (playerCanMove == true)
        {
            oxygenLevel -= oxygenUseRate * Time.unscaledDeltaTime;
            oxygenLevel = Mathf.Clamp(oxygenLevel, 0.0f, 100.0f);
            UIManager.curUIManager.UpdateOxygenBar(oxygenLevel / 100.0f);
            if(oxygenLevel <= 0.0f)
            {
                GameManager.curGameManager.TriggerPlayerDeath(true, 1);
                return;
            }

            if (fingerDown == true )// && GameManager.curGameManager.useVirtualJoystick == false) //TODO
            {
                Vector3 tapPos = Vector3.zero;

                if (touchInputDetected == false)
                {
                    tapPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                }
                else
                {
                    tapPos = Camera.main.ScreenToWorldPoint(Input.touches[0].position);
                }
                Vector3 _wantRot = tapPos - thisTransform.position;
                float _rotz = Mathf.Atan2(_wantRot.y, _wantRot.x) * Mathf.Rad2Deg;
                Quaternion targetRot = Quaternion.Euler(0, 0, _rotz - 90);
                if (checkForSpeedChange == true && moveSpeed > 1.5f)
                {
                    checkForSpeedChange = false;
                    float diff = Quaternion.Angle(thisTransform.rotation, targetRot);
                    float speedEffect = (moveSpeed / 2.0f) / 180.0f;
                    speedEffect *= diff;
                    moveSpeed -= speedEffect;
                }
                thisTransform.rotation = Quaternion.Slerp(thisTransform.rotation, targetRot, Time.deltaTime * shipRotationSpeed);
            }

            myTime += Time.deltaTime;
            thisTransform.Translate(0, -moveSpeed * Time.deltaTime, 0, Space.Self);

            if (applyAcceleration == false && moveSpeed >= floatSpeed)
            {
                moveSpeed -= slowDownMoveMultiplyer * Time.deltaTime;
                EnemySpawner.curEnemySpawner.SpawnTimeUpdate(moveSpeed);
                GameManager.curGameManager.SetScoreMultiplyer(moveSpeed);
            }
            else if (applyAcceleration == false && moveSpeed < 0)
            {
                moveSpeed += slowDownMoveMultiplyer * Time.deltaTime;
                EnemySpawner.curEnemySpawner.SpawnTimeUpdate(moveSpeed);
                GameManager.curGameManager.SetScoreMultiplyer(moveSpeed);
            }
        }

        //TODO - must find a better way to do this! this shit be crazy!! (damn new particle system)
        //if (shieldCooldown == true)
        //{
        //    //var _col = psMain.colorOverLifetime;
        //    //Gradient grad = new Gradient();

        //    currentShieldCooldownTime -= Time.deltaTime;
        //    if (currentShieldCooldownTime <= 0.0f)
        //    {
        //       // shieldAlpha = 1.0f;
        //        shieldCooldown = false;
        //        ActivateShield(false);
        //        currentShieldCooldownTime = shieldCooldownTime;

        //        //grad.SetKeys(new GradientColorKey[] { new GradientColorKey(Color.white, 0.0f), new GradientColorKey(Color.white, 0.59f), new GradientColorKey(Color.white, 1.0f) }, new GradientAlphaKey[] { new GradientAlphaKey(1.0f, 0.0f), new GradientAlphaKey(1.0f, 0.59f), new GradientAlphaKey(0.0f, 1.0f) });
        //    }
        //    //else
        //    //{
        //    //    shieldAlpha = 0.5f + Mathf.Sin(Time.fixedTime * Mathf.PI * (shieldCooldownTime * 3));
        //    //    shieldAlpha = Mathf.Clamp(shieldAlpha, 0.0f, 1.0f);

        //    //    grad.SetKeys(new GradientColorKey[] { new GradientColorKey(Color.white, 0.0f), new GradientColorKey(Color.white, 0.59f), new GradientColorKey(Color.white, 1.0f) }, new GradientAlphaKey[] { new GradientAlphaKey(shieldAlpha, 0.0f), new GradientAlphaKey(shieldAlpha, 0.59f), new GradientAlphaKey(0.0f, 1.0f) });
        //    //}

        //    //_col.color = grad;

        //}
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        if(!other.gameObject.activeInHierarchy)
        {
            return;
        }        

        Entity e = other.GetComponent<Entity>();
        if (e != null)
        {
            if (e.ItemStats.myEntityType == EntityType.Asteroid)
            {
                camShake.ShakeDuration = shieldActive == true ? camShakeDuration / 2.0f : camShakeDuration;
                camShake.ShakeAmount = shieldActive == true ? amountToShakeCam / 2.0f : amountToShakeCam;
                camShake.ShakeCamera();

                if (shieldActive == true)
                {
                    e.TakeDamage(crashDamage);
                }
                else
                {
                    myTime = 0;
                    fingerDown = false;
                    applyAcceleration = false;
                    GameManager.curGameManager.TriggerPlayerDeath(true, 0);

                }

                return;
            }
        }
        if(other.CompareTag("Pickup"))
        {
            other.GetComponent<Pickup>().OnPlayerCollect();
        }
        
    }

    public void ActivateShield(bool state)
    {
       // shieldAlpha = 1.0f;

        if (state == true)
        {
            if(shieldAnim == null)
            {
                shieldAnim = gameObject.GetComponentInChildren<Animator>();
                shieldSpriteRnd = shieldAnim.gameObject.GetComponent<SpriteRenderer>();
            }
            
            //shieldCooldown = false;
            shieldActive = true;
            shieldAnim.enabled = true;
            shieldAnim.SetInteger("ShipID", shieldAnimationID);
            shieldSpriteRnd.enabled = true;
            //shieldParticleEffect.SetActive(true);
            //shieldOutline.SetActive(true);
            //shieldBounds.SetActive(true); //TODO - re-enable shield
        }
        else
        {
            //currentShieldCooldownTime = shieldCooldownTime;
            shieldActive = false;
            if (shieldAnim == null)
            {
                shieldAnim = gameObject.GetComponentInChildren<Animator>();
                shieldSpriteRnd = shieldAnim.gameObject.GetComponent<SpriteRenderer>();
            }
            shieldSpriteRnd.enabled = false;
            shieldAnim.enabled = false;
            //shieldParticleEffect.SetActive(false);
            //shieldOutline.SetActive(false);
            //shieldBounds.SetActive(false); //TODO - re-enable shield
        }
    }

    private void OnDisable()
    {
        InputManager.OnTap -= InputManager_OnTap;
        InputManager.OnTapEnd -= InputManager_OnTapEnd;
    }
}
