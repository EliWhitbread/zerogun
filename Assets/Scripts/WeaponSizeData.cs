﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Pickups/WeaponSizeData")]
public class WeaponSizeData : ScriptableObject {

    public bool isBigShot;
}
