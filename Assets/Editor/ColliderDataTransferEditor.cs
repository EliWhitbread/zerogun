﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ColliderDataTransfer))]
public class ColliderDataTransferEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        ColliderDataTransfer dataTransfer = (ColliderDataTransfer)target;

        if (GUILayout.Button("Transfer Data"))
        {
            dataTransfer.TransferColliderData();
        }
    }
    
}
