﻿Shader "ZeroGun/Laser"
{
	Properties
	{
		_MainTex ("Diffuse Texture", 2D) = "White" {}
		_Colour ("Colour" , Color) = (1, 1, 1, 1)
	}

		Subshader
	{
		CGPROGRAM
			#pragma surface surf Lambert

			sampler2D _MainTex;
			float4 _Colour;
			
			struct Input
			{
				float2 uv_MainTex;
			};

			void surf(Input IN, inout SurfaceOutput o)
			{
				o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb * _Colour.rgb;
			}
		ENDCG
	}
	Fallback "Diffuse"
}